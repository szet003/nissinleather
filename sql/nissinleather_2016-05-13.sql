# ************************************************************
# Sequel Pro SQL dump
# Version 4135
#
# http://www.sequelpro.com/
# http://code.google.com/p/sequel-pro/
#
# Host: localhost (MySQL 5.5.42)
# Database: nissinleather
# Generation Time: 2016-05-13 07:15:20 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table nssle_setting
# ------------------------------------------------------------

DROP TABLE IF EXISTS `nssle_setting`;

CREATE TABLE `nssle_setting` (
  `setting_id` int(11) NOT NULL AUTO_INCREMENT,
  `store_id` int(11) NOT NULL DEFAULT '0',
  `code` varchar(32) NOT NULL,
  `key` varchar(64) NOT NULL,
  `value` text NOT NULL,
  `serialized` tinyint(1) NOT NULL,
  PRIMARY KEY (`setting_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `nssle_setting` WRITE;
/*!40000 ALTER TABLE `nssle_setting` DISABLE KEYS */;

INSERT INTO `nssle_setting` (`setting_id`, `store_id`, `code`, `key`, `value`, `serialized`)
VALUES
	(1,0,'shipping','shipping_sort_order','3',0),
	(2,0,'sub_total','sub_total_sort_order','1',0),
	(3,0,'sub_total','sub_total_status','1',0),
	(4,0,'tax','tax_status','1',0),
	(5,0,'total','total_sort_order','9',0),
	(6,0,'total','total_status','1',0),
	(7,0,'tax','tax_sort_order','5',0),
	(8,0,'free_checkout','free_checkout_sort_order','1',0),
	(9,0,'cod','cod_sort_order','5',0),
	(10,0,'cod','cod_total','0.01',0),
	(11,0,'cod','cod_order_status_id','1',0),
	(12,0,'cod','cod_geo_zone_id','0',0),
	(13,0,'cod','cod_status','1',0),
	(14,0,'shipping','shipping_status','1',0),
	(15,0,'shipping','shipping_estimator','1',0),
	(27,0,'coupon','coupon_sort_order','4',0),
	(28,0,'coupon','coupon_status','1',0),
	(34,0,'flat','flat_sort_order','1',0),
	(35,0,'flat','flat_status','1',0),
	(36,0,'flat','flat_geo_zone_id','0',0),
	(37,0,'flat','flat_tax_class_id','9',0),
	(41,0,'flat','flat_cost','5.00',0),
	(42,0,'credit','credit_sort_order','7',0),
	(43,0,'credit','credit_status','1',0),
	(53,0,'reward','reward_sort_order','2',0),
	(54,0,'reward','reward_status','1',0),
	(146,0,'category','category_status','1',0),
	(158,0,'account','account_status','1',0),
	(159,0,'affiliate','affiliate_status','1',0),
	(645,0,'pages_content_blog','pages_content_blog_status','1',0),
	(94,0,'voucher','voucher_sort_order','8',0),
	(95,0,'voucher','voucher_status','1',0),
	(103,0,'free_checkout','free_checkout_status','1',0),
	(104,0,'free_checkout','free_checkout_order_status_id','1',0),
	(1596,0,'config','config_compression','0',0),
	(1597,0,'config','config_secure','0',0),
	(1598,0,'config','config_password','1',0),
	(1599,0,'config','config_shared','0',0),
	(1600,0,'config','config_encryption','svIDTXhWjWDJ4mQs3tmBqABLaQOnO3ab7dExJRD7Yb4dzI8O7eNncLwBmkdwk22qEJMLEfQevSllaLVLfl0OW4hrskp279qT4XIxeMrpGM9LOQPDl5RwkVeeh4atF4I82M2R4EksvrxFDT8BgWFgZZztONKCgy0YSIBc3sTc2K0YzFDRVxFOg08qYB5wAngSy2HM9J1nLjoSFYFRgArKGlZU9p7YUch2YZs72SiZppxU9nw6HQAxCm1h9lkd9HOmo94VfJKCN8yGsFwZikAKF4rv9Pelaro9yo1JyE2FrsGDlnuZSazSRmQrmDdzSmgjyIzDfb8dFoGjzSDeuEZMfkgzAW5xZuf7KoO8wjOykKmTNiOEXHZNCvoDpn5c7HM7Vq44wvlZoImptau3fmv3XNhU1uDlaRL1WaD5C6EpifOzxLKY51hhdFk9h39yVL6SOZRrA9esFTbmCTx7jFN3YNpmSqwNzNjc1foMh0tZsCaM7iBd6qlG6f4tKXQySd8wXmZywabYsK8Uklxee7pshzlthstTcG61EBmyQyszzS95ssXUYKQkV15bsbMddmgqZwM1wvAXkjaXMQaWq6EaENUUzHBTkhDtGzRxfSyiN8XuTVl0gQG4OViLvaCuXDvhBpBBcbIaek7OuMHNbPr7fxNeGvbmsa1gHKWeWwyulQ6cRgXA1yQU9pfqcPIEOWgeBARgiQ0gE5HVztGGYWacZfwnBQMsYtj2HzzmzmC6HqF6Mz6pibcsE8RPDfiJWT0oMDF573GH9ujtuqOUDaTGWsXDc3xIGSMmFfbHsb5VjV8NnQyQq2jBcgkLgnMrFo7zCAaDjDceWbH8Q88PlWHsE7a65dxhnGWak7oCAxtqLPxrTh4PaACfB9O1YeyCfGvnS83LPwlMAZH6rrJwA531JkieMllgPwvwChevCKqPwEGPsYiu201JllpwteCxim2MF3WxSqQeiq8xiRlTh2JFSevBN9O7CYEw9H73xgSAchGscv1Pdj8ouTmBYlwjKYbp',0),
	(1582,0,'config','config_ftp_password','',0),
	(1583,0,'config','config_ftp_root','',0),
	(1584,0,'config','config_ftp_status','0',0),
	(1585,0,'config','config_mail_protocol','mail',0),
	(1586,0,'config','config_mail_parameter','',0),
	(1587,0,'config','config_mail_smtp_hostname','',0),
	(1588,0,'config','config_mail_smtp_username','',0),
	(1589,0,'config','config_mail_smtp_password','',0),
	(1590,0,'config','config_mail_smtp_port','25',0),
	(1591,0,'config','config_mail_smtp_timeout','5',0),
	(1592,0,'config','config_mail_alert','',0),
	(1593,0,'config','config_maintenance','0',0),
	(1594,0,'config','config_seo_url','1',0),
	(1595,0,'config','config_robots','abot\r\ndbot\r\nebot\r\nhbot\r\nkbot\r\nlbot\r\nmbot\r\nnbot\r\nobot\r\npbot\r\nrbot\r\nsbot\r\ntbot\r\nvbot\r\nybot\r\nzbot\r\nbot.\r\nbot/\r\n_bot\r\n.bot\r\n/bot\r\n-bot\r\n:bot\r\n(bot\r\ncrawl\r\nslurp\r\nspider\r\nseek\r\naccoona\r\nacoon\r\nadressendeutschland\r\nah-ha.com\r\nahoy\r\naltavista\r\nananzi\r\nanthill\r\nappie\r\narachnophilia\r\narale\r\naraneo\r\naranha\r\narchitext\r\naretha\r\narks\r\nasterias\r\natlocal\r\natn\r\natomz\r\naugurfind\r\nbackrub\r\nbannana_bot\r\nbaypup\r\nbdfetch\r\nbig brother\r\nbiglotron\r\nbjaaland\r\nblackwidow\r\nblaiz\r\nblog\r\nblo.\r\nbloodhound\r\nboitho\r\nbooch\r\nbradley\r\nbutterfly\r\ncalif\r\ncassandra\r\nccubee\r\ncfetch\r\ncharlotte\r\nchurl\r\ncienciaficcion\r\ncmc\r\ncollective\r\ncomagent\r\ncombine\r\ncomputingsite\r\ncsci\r\ncurl\r\ncusco\r\ndaumoa\r\ndeepindex\r\ndelorie\r\ndepspid\r\ndeweb\r\ndie blinde kuh\r\ndigger\r\nditto\r\ndmoz\r\ndocomo\r\ndownload express\r\ndtaagent\r\ndwcp\r\nebiness\r\nebingbong\r\ne-collector\r\nejupiter\r\nemacs-w3 search engine\r\nesther\r\nevliya celebi\r\nezresult\r\nfalcon\r\nfelix ide\r\nferret\r\nfetchrover\r\nfido\r\nfindlinks\r\nfireball\r\nfish search\r\nfouineur\r\nfunnelweb\r\ngazz\r\ngcreep\r\ngenieknows\r\ngetterroboplus\r\ngeturl\r\nglx\r\ngoforit\r\ngolem\r\ngrabber\r\ngrapnel\r\ngralon\r\ngriffon\r\ngromit\r\ngrub\r\ngulliver\r\nhamahakki\r\nharvest\r\nhavindex\r\nhelix\r\nheritrix\r\nhku www octopus\r\nhomerweb\r\nhtdig\r\nhtml index\r\nhtml_analyzer\r\nhtmlgobble\r\nhubater\r\nhyper-decontextualizer\r\nia_archiver\r\nibm_planetwide\r\nichiro\r\niconsurf\r\niltrovatore\r\nimage.kapsi.net\r\nimagelock\r\nincywincy\r\nindexer\r\ninfobee\r\ninformant\r\ningrid\r\ninktomisearch.com\r\ninspector web\r\nintelliagent\r\ninternet shinchakubin\r\nip3000\r\niron33\r\nisraeli-search\r\nivia\r\njack\r\njakarta\r\njavabee\r\njetbot\r\njumpstation\r\nkatipo\r\nkdd-explorer\r\nkilroy\r\nknowledge\r\nkototoi\r\nkretrieve\r\nlabelgrabber\r\nlachesis\r\nlarbin\r\nlegs\r\nlibwww\r\nlinkalarm\r\nlink validator\r\nlinkscan\r\nlockon\r\nlwp\r\nlycos\r\nmagpie\r\nmantraagent\r\nmapoftheinternet\r\nmarvin/\r\nmattie\r\nmediafox\r\nmediapartners\r\nmercator\r\nmerzscope\r\nmicrosoft url control\r\nminirank\r\nmiva\r\nmj12\r\nmnogosearch\r\nmoget\r\nmonster\r\nmoose\r\nmotor\r\nmultitext\r\nmuncher\r\nmuscatferret\r\nmwd.search\r\nmyweb\r\nnajdi\r\nnameprotect\r\nnationaldirectory\r\nnazilla\r\nncsa beta\r\nnec-meshexplorer\r\nnederland.zoek\r\nnetcarta webmap engine\r\nnetmechanic\r\nnetresearchserver\r\nnetscoop\r\nnewscan-online\r\nnhse\r\nnokia6682/\r\nnomad\r\nnoyona\r\nnutch\r\nnzexplorer\r\nobjectssearch\r\noccam\r\nomni\r\nopen text\r\nopenfind\r\nopenintelligencedata\r\norb search\r\nosis-project\r\npack rat\r\npageboy\r\npagebull\r\npage_verifier\r\npanscient\r\nparasite\r\npartnersite\r\npatric\r\npear.\r\npegasus\r\nperegrinator\r\npgp key agent\r\nphantom\r\nphpdig\r\npicosearch\r\npiltdownman\r\npimptrain\r\npinpoint\r\npioneer\r\npiranha\r\nplumtreewebaccessor\r\npogodak\r\npoirot\r\npompos\r\npoppelsdorf\r\npoppi\r\npopular iconoclast\r\npsycheclone\r\npublisher\r\npython\r\nrambler\r\nraven search\r\nroach\r\nroad runner\r\nroadhouse\r\nrobbie\r\nrobofox\r\nrobozilla\r\nrules\r\nsalty\r\nsbider\r\nscooter\r\nscoutjet\r\nscrubby\r\nsearch.\r\nsearchprocess\r\nsemanticdiscovery\r\nsenrigan\r\nsg-scout\r\nshai\'hulud\r\nshark\r\nshopwiki\r\nsidewinder\r\nsift\r\nsilk\r\nsimmany\r\nsite searcher\r\nsite valet\r\nsitetech-rover\r\nskymob.com\r\nsleek\r\nsmartwit\r\nsna-\r\nsnappy\r\nsnooper\r\nsohu\r\nspeedfind\r\nsphere\r\nsphider\r\nspinner\r\nspyder\r\nsteeler/\r\nsuke\r\nsuntek\r\nsupersnooper\r\nsurfnomore\r\nsven\r\nsygol\r\nszukacz\r\ntach black widow\r\ntarantula\r\ntempleton\r\n/teoma\r\nt-h-u-n-d-e-r-s-t-o-n-e\r\ntheophrastus\r\ntitan\r\ntitin\r\ntkwww\r\ntoutatis\r\nt-rex\r\ntutorgig\r\ntwiceler\r\ntwisted\r\nucsd\r\nudmsearch\r\nurl check\r\nupdated\r\nvagabondo\r\nvalkyrie\r\nverticrawl\r\nvictoria\r\nvision-search\r\nvolcano\r\nvoyager/\r\nvoyager-hc\r\nw3c_validator\r\nw3m2\r\nw3mir\r\nwalker\r\nwallpaper\r\nwanderer\r\nwauuu\r\nwavefire\r\nweb core\r\nweb hopper\r\nweb wombat\r\nwebbandit\r\nwebcatcher\r\nwebcopy\r\nwebfoot\r\nweblayers\r\nweblinker\r\nweblog monitor\r\nwebmirror\r\nwebmonkey\r\nwebquest\r\nwebreaper\r\nwebsitepulse\r\nwebsnarf\r\nwebstolperer\r\nwebvac\r\nwebwalk\r\nwebwatch\r\nwebwombat\r\nwebzinger\r\nwhizbang\r\nwhowhere\r\nwild ferret\r\nworldlight\r\nwwwc\r\nwwwster\r\nxenu\r\nxget\r\nxift\r\nxirq\r\nyandex\r\nyanga\r\nyeti\r\nyodao\r\nzao\r\nzippp\r\nzyborg',0),
	(1573,0,'config','config_return_id','0',0),
	(1574,0,'config','config_return_status_id','2',0),
	(1575,0,'config','config_captcha','',0),
	(1576,0,'config','config_captcha_page','[\"review\",\"return\",\"contact\"]',1),
	(1577,0,'config','config_logo','catalog/nguyen duc anh.png',0),
	(1578,0,'config','config_icon','catalog/cart.png',0),
	(1579,0,'config','config_ftp_hostname','nissinleather.localhost',0),
	(1580,0,'config','config_ftp_port','21',0),
	(1581,0,'config','config_ftp_username','',0),
	(1572,0,'config','config_affiliate_mail','0',0),
	(1571,0,'config','config_affiliate_id','4',0),
	(1570,0,'config','config_affiliate_commission','5',0),
	(1569,0,'config','config_affiliate_auto','0',0),
	(1568,0,'config','config_affiliate_approval','0',0),
	(1567,0,'config','config_stock_checkout','0',0),
	(1566,0,'config','config_stock_warning','0',0),
	(1565,0,'config','config_stock_display','0',0),
	(1564,0,'config','config_api_id','1',0),
	(1563,0,'config','config_order_mail','0',0),
	(1562,0,'config','config_fraud_status_id','7',0),
	(1561,0,'config','config_complete_status','[\"5\",\"3\"]',1),
	(1560,0,'config','config_processing_status','[\"5\",\"1\",\"2\",\"12\",\"3\"]',1),
	(1559,0,'config','config_order_status_id','1',0),
	(1558,0,'config','config_checkout_id','5',0),
	(1557,0,'config','config_checkout_guest','1',0),
	(1556,0,'config','config_cart_weight','1',0),
	(1514,0,'theme_default','theme_default_image_location_height','50',0),
	(1513,0,'theme_default','theme_default_image_location_width','268',0),
	(1512,0,'theme_default','theme_default_image_cart_height','47',0),
	(1511,0,'theme_default','theme_default_image_cart_width','47',0),
	(1510,0,'theme_default','theme_default_image_wishlist_height','47',0),
	(1509,0,'theme_default','theme_default_image_wishlist_width','47',0),
	(1508,0,'theme_default','theme_default_image_compare_height','90',0),
	(1507,0,'theme_default','theme_default_image_compare_width','90',0),
	(1506,0,'theme_default','theme_default_image_related_height','200',0),
	(1555,0,'config','config_invoice_prefix','INV-2013-00',0),
	(1554,0,'config','config_account_mail','0',0),
	(1553,0,'config','config_account_id','3',0),
	(1552,0,'config','config_login_attempts','5',0),
	(1551,0,'config','config_customer_price','0',0),
	(1550,0,'config','config_customer_group_display','[\"1\"]',1),
	(1549,0,'config','config_customer_group_id','1',0),
	(1548,0,'config','config_customer_online','0',0),
	(1547,0,'config','config_tax_customer','shipping',0),
	(1546,0,'config','config_tax_default','shipping',0),
	(1545,0,'config','config_tax','1',0),
	(1544,0,'config','config_voucher_max','1000',0),
	(1543,0,'config','config_voucher_min','1',0),
	(1542,0,'config','config_review_mail','0',0),
	(1541,0,'config','config_review_guest','1',0),
	(1540,0,'config','config_review_status','1',0),
	(1539,0,'config','config_limit_admin','20',0),
	(1538,0,'config','config_product_count','1',0),
	(1537,0,'config','config_weight_class_id','1',0),
	(1536,0,'config','config_length_class_id','1',0),
	(1535,0,'config','config_currency_auto','1',0),
	(1534,0,'config','config_currency','USD',0),
	(1533,0,'config','config_admin_language','en-gb',0),
	(1502,0,'theme_default','theme_default_big_image_product_height','827',0),
	(1503,0,'theme_default','theme_default_image_additional_width','1200',0),
	(648,0,'pages_small_content','pages_small_content_status','1',0),
	(649,0,'contact_form','contact_form_status','1',0),
	(1532,0,'config','config_language','en-gb',0),
	(1531,0,'config','config_zone_id','3776',0),
	(1530,0,'config','config_country_id','230',0),
	(1529,0,'config','config_comment','',0),
	(1528,0,'config','config_open','',0),
	(1527,0,'config','config_image','',0),
	(1526,0,'config','config_fax','',0),
	(1525,0,'config','config_telephone','123456789',0),
	(1524,0,'config','config_email','admin@admin.com',0),
	(1523,0,'config','config_geocode','',0),
	(1522,0,'config','config_address','Address 1',0),
	(1521,0,'config','config_owner','Nissin Leather',0),
	(1520,0,'config','config_name','Nissin Leather',0),
	(1519,0,'config','config_layout_id','4',0),
	(1518,0,'config','config_theme','theme_default',0),
	(1517,0,'config','config_meta_keyword','',0),
	(1516,0,'config','config_meta_description','My Store',0),
	(1515,0,'config','config_meta_title','Nissin Leather',0),
	(1505,0,'theme_default','theme_default_image_related_width','200',0),
	(1504,0,'theme_default','theme_default_image_additional_height','800',0),
	(1500,0,'theme_default','theme_default_image_product_height','480',0),
	(1501,0,'theme_default','theme_default_big_image_product_width','1891',0),
	(1498,0,'theme_default','theme_default_image_popup_height','500',0),
	(1499,0,'theme_default','theme_default_image_product_width','480',0),
	(1497,0,'theme_default','theme_default_image_popup_width','500',0),
	(1496,0,'theme_default','theme_default_image_thumb_height','228',0),
	(1494,0,'theme_default','theme_default_image_category_height','80',0),
	(1495,0,'theme_default','theme_default_image_thumb_width','228',0),
	(1493,0,'theme_default','theme_default_image_category_width','80',0),
	(1492,0,'theme_default','theme_default_product_description_length','100',0),
	(1491,0,'theme_default','theme_default_product_limit','32',0),
	(1490,0,'theme_default','theme_default_status','1',0),
	(1489,0,'theme_default','theme_default_directory','nissinleather',0),
	(1601,0,'config','config_file_max_size','300000',0),
	(1602,0,'config','config_file_ext_allowed','zip\r\ntxt\r\npng\r\njpe\r\njpeg\r\njpg\r\ngif\r\nbmp\r\nico\r\ntiff\r\ntif\r\nsvg\r\nsvgz\r\nzip\r\nrar\r\nmsi\r\ncab\r\nmp3\r\nqt\r\nmov\r\npdf\r\npsd\r\nai\r\neps\r\nps\r\ndoc',0),
	(1603,0,'config','config_file_mime_allowed','text/plain\r\nimage/png\r\nimage/jpeg\r\nimage/gif\r\nimage/bmp\r\nimage/tiff\r\nimage/svg+xml\r\napplication/zip\r\n&quot;application/zip&quot;\r\napplication/x-zip\r\n&quot;application/x-zip&quot;\r\napplication/x-zip-compressed\r\n&quot;application/x-zip-compressed&quot;\r\napplication/rar\r\n&quot;application/rar&quot;\r\napplication/x-rar\r\n&quot;application/x-rar&quot;\r\napplication/x-rar-compressed\r\n&quot;application/x-rar-compressed&quot;\r\napplication/octet-stream\r\n&quot;application/octet-stream&quot;\r\naudio/mpeg\r\nvideo/quicktime\r\napplication/pdf',0),
	(1604,0,'config','config_error_display','1',0),
	(1605,0,'config','config_error_log','1',0),
	(1606,0,'config','config_error_filename','error.log',0);

/*!40000 ALTER TABLE `nssle_setting` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
