<?php
// Heading
$_['heading_title']    = 'Pages Small Content';

// Text
$_['text_module']      = 'Modules';
$_['text_success']     = 'Success: You have modified this module!';
$_['text_edit']        = 'Edit Module';

// Entry
$_['entry_status']     = 'Status';

// Error
$_['error_permission'] = 'Warning: You do not have permission to modify this module!';