<?php
// Heading
$_['heading_title']    = 'List Pages Children by Pages ID';

// Text
$_['text_module']      = 'Modules';
$_['text_success']     = 'Success: You have modified this module!';
$_['text_edit']        = 'Edit Category Module';
$_['text_type_row']        = 'Row';
$_['text_type_block']        = 'Block';

// Entry
$_['entry_status']     = 'Status';
$_['entry_name']     = 'Name';
$_['entry_type']     = 'Display type';

// Error
$_['error_permission'] = 'Warning: You do not have permission to modify this module!';