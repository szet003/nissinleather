<?php

class ControllerCatalogPages extends Controller
{
    private $error = array();

    public function index()
    {
        $this->load->language('catalog/pages');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('catalog/pages');

        $this->getList();
    }

    public function add()
    {
        $this->load->language('catalog/pages');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('catalog/pages');

        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
            $this->model_catalog_pages->addpages($this->request->post);

            $this->session->data['success'] = $this->language->get('text_success');

            $url = '';

            if (isset($this->request->get['sort'])) {
                $url .= '&sort=' . $this->request->get['sort'];
            }

            if (isset($this->request->get['order'])) {
                $url .= '&order=' . $this->request->get['order'];
            }

            if (isset($this->request->get['page'])) {
                $url .= '&page=' . $this->request->get['page'];
            }

            $this->response->redirect($this->url->link('catalog/pages', 'token=' . $this->session->data['token'] . $url, true));
        }

        $this->getForm();
    }

    public function edit()
    {
        $this->load->language('catalog/pages');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('catalog/pages');

        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
            $this->model_catalog_pages->editpages($this->request->get['pages_id'], $this->request->post);

            $this->session->data['success'] = $this->language->get('text_success');

            $url = '';

            if (isset($this->request->get['sort'])) {
                $url .= '&sort=' . $this->request->get['sort'];
            }

            if (isset($this->request->get['order'])) {
                $url .= '&order=' . $this->request->get['order'];
            }

            if (isset($this->request->get['page'])) {
                $url .= '&page=' . $this->request->get['page'];
            }

            $this->response->redirect($this->url->link('catalog/pages', 'token=' . $this->session->data['token'] . $url, true));
        }

        $this->getForm();
    }

    public function delete()
    {
        $this->load->language('catalog/pages');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('catalog/pages');

        if (isset($this->request->post['selected']) && $this->validateDelete()) {
            foreach ($this->request->post['selected'] as $pages_id) {
                $this->model_catalog_pages->deletepages($pages_id);
            }

            $this->session->data['success'] = $this->language->get('text_success');

            $url = '';

            if (isset($this->request->get['sort'])) {
                $url .= '&sort=' . $this->request->get['sort'];
            }

            if (isset($this->request->get['order'])) {
                $url .= '&order=' . $this->request->get['order'];
            }

            if (isset($this->request->get['page'])) {
                $url .= '&page=' . $this->request->get['page'];
            }

            $this->response->redirect($this->url->link('catalog/pages', 'token=' . $this->session->data['token'] . $url, true));
        }

        $this->getList();
    }

    public function repair()
    {
        $this->load->language('catalog/pages');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('catalog/pages');

        if ($this->validateRepair()) {
            $this->model_catalog_pages->repairPagess();

            $this->session->data['success'] = $this->language->get('text_success');

            $this->response->redirect($this->url->link('catalog/pages', 'token=' . $this->session->data['token'], true));
        }

        $this->getList();
    }

    protected function getList()
    {
        if (isset($this->request->get['sort'])) {
            $sort = $this->request->get['sort'];
        } else {
            $sort = 'name';
        }

        if (isset($this->request->get['order'])) {
            $order = $this->request->get['order'];
        } else {
            $order = 'ASC';
        }

        if (isset($this->request->get['page'])) {
            $page = $this->request->get['page'];
        } else {
            $page = 1;
        }

        $url = '';

        if (isset($this->request->get['sort'])) {
            $url .= '&sort=' . $this->request->get['sort'];
        }

        if (isset($this->request->get['order'])) {
            $url .= '&order=' . $this->request->get['order'];
        }

        if (isset($this->request->get['page'])) {
            $url .= '&page=' . $this->request->get['page'];
        }

        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title'),
            'href' => $this->url->link('catalog/pages', 'token=' . $this->session->data['token'] . $url, true)
        );

        $data['add'] = $this->url->link('catalog/pages/add', 'token=' . $this->session->data['token'] . $url, true);
        $data['delete'] = $this->url->link('catalog/pages/delete', 'token=' . $this->session->data['token'] . $url, true);
        $data['repair'] = $this->url->link('catalog/pages/repair', 'token=' . $this->session->data['token'] . $url, true);

        $data['pagess'] = array();

        $filter_data = array(
            'sort' => $sort,
            'order' => $order,
            'start' => ($page - 1) * $this->config->get('config_limit_admin'),
            'limit' => $this->config->get('config_limit_admin')
        );

        $pages_total = $this->model_catalog_pages->getTotalPagess();

        $results = $this->model_catalog_pages->getPagess($filter_data);

        foreach ($results as $result) {
            $data['pagess'][] = array(
                'pages_id' => $result['pages_id'],
                'name' => $result['name'],
                'sort_order' => $result['sort_order'],
                'edit' => $this->url->link('catalog/pages/edit', 'token=' . $this->session->data['token'] . '&pages_id=' . $result['pages_id'] . $url, true),
                'delete' => $this->url->link('catalog/pages/delete', 'token=' . $this->session->data['token'] . '&pages_id=' . $result['pages_id'] . $url, true)
            );
        }

        $data['heading_title'] = $this->language->get('heading_title');

        $data['text_list'] = $this->language->get('text_list');
        $data['text_no_results'] = $this->language->get('text_no_results');
        $data['text_confirm'] = $this->language->get('text_confirm');

        $data['column_name'] = $this->language->get('column_name');
        $data['column_sort_order'] = $this->language->get('column_sort_order');
        $data['column_action'] = $this->language->get('column_action');

        $data['button_add'] = $this->language->get('button_add');
        $data['button_edit'] = $this->language->get('button_edit');
        $data['button_delete'] = $this->language->get('button_delete');
        $data['button_rebuild'] = $this->language->get('button_rebuild');

        if (isset($this->error['warning'])) {
            $data['error_warning'] = $this->error['warning'];
        } else {
            $data['error_warning'] = '';
        }

        if (isset($this->session->data['success'])) {
            $data['success'] = $this->session->data['success'];

            unset($this->session->data['success']);
        } else {
            $data['success'] = '';
        }

        if (isset($this->request->post['selected'])) {
            $data['selected'] = (array)$this->request->post['selected'];
        } else {
            $data['selected'] = array();
        }

        $url = '';

        if ($order == 'ASC') {
            $url .= '&order=DESC';
        } else {
            $url .= '&order=ASC';
        }

        if (isset($this->request->get['page'])) {
            $url .= '&page=' . $this->request->get['page'];
        }

        $data['sort_name'] = $this->url->link('catalog/pages', 'token=' . $this->session->data['token'] . '&sort=name' . $url, true);
        $data['sort_sort_order'] = $this->url->link('catalog/pages', 'token=' . $this->session->data['token'] . '&sort=sort_order' . $url, true);

        $url = '';

        if (isset($this->request->get['sort'])) {
            $url .= '&sort=' . $this->request->get['sort'];
        }

        if (isset($this->request->get['order'])) {
            $url .= '&order=' . $this->request->get['order'];
        }

        $pagination = new Pagination();
        $pagination->total = $pages_total;
        $pagination->page = $page;
        $pagination->limit = $this->config->get('config_limit_admin');
        $pagination->url = $this->url->link('catalog/pages', 'token=' . $this->session->data['token'] . $url . '&page={page}', true);

        $data['pagination'] = $pagination->render();

        $data['results'] = sprintf($this->language->get('text_pagination'), ($pages_total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($pages_total - $this->config->get('config_limit_admin'))) ? $pages_total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $pages_total, ceil($pages_total / $this->config->get('config_limit_admin')));

        $data['sort'] = $sort;
        $data['order'] = $order;

        $data['header'] = $this->load->controller('common/header');
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['footer'] = $this->load->controller('common/footer');

        $this->response->setOutput($this->load->view('catalog/pages_list', $data));
    }

    protected function getForm()
    {
        $data['heading_title'] = $this->language->get('heading_title');

        $data['text_form'] = !isset($this->request->get['pages_id']) ? $this->language->get('text_add') : $this->language->get('text_edit');
        $data['text_none'] = $this->language->get('text_none');
        $data['text_default'] = $this->language->get('text_default');
        $data['text_enabled'] = $this->language->get('text_enabled');
        $data['text_disabled'] = $this->language->get('text_disabled');

        $data['entry_name'] = $this->language->get('entry_name');
        $data['entry_description'] = $this->language->get('entry_description');
        $data['entry_description_small'] = $this->language->get('entry_description_small');
        $data['entry_meta_title'] = $this->language->get('entry_meta_title');
        $data['entry_meta_description'] = $this->language->get('entry_meta_description');
        $data['entry_meta_keyword'] = $this->language->get('entry_meta_keyword');
        $data['entry_keyword'] = $this->language->get('entry_keyword');
        $data['entry_parent'] = $this->language->get('entry_parent');
        $data['entry_filter'] = $this->language->get('entry_filter');
        $data['entry_store'] = $this->language->get('entry_store');
        $data['entry_image'] = $this->language->get('entry_image');
        $data['entry_top'] = $this->language->get('entry_top');
        $data['entry_home'] = $this->language->get('entry_home');
        $data['entry_using_breadcrumbs'] = $this->language->get('entry_using_breadcrumbs');
        $data['entry_column'] = $this->language->get('entry_column');
        $data['entry_sort_order'] = $this->language->get('entry_sort_order');
        $data['entry_status'] = $this->language->get('entry_status');
        $data['entry_layout'] = $this->language->get('entry_layout');
        $data['entry_caption'] = $this->language->get('entry_caption');
        $data['entry_sub_title'] = $this->language->get('entry_sub_title');
        $data['entry_description_small'] = $this->language->get('entry_description_small');
        $data['entry_full_page'] = $this->language->get('entry_full_page');

        $data['help_filter'] = $this->language->get('help_filter');
        $data['help_keyword'] = $this->language->get('help_keyword');
        $data['help_top'] = $this->language->get('help_top');
        $data['help_column'] = $this->language->get('help_column');

        $data['button_save'] = $this->language->get('button_save');
        $data['button_cancel'] = $this->language->get('button_cancel');
        $data['button_image_add'] = $this->language->get('button_image_add');

        $data['tab_general'] = $this->language->get('tab_general');
        $data['tab_data'] = $this->language->get('tab_data');
        $data['tab_design'] = $this->language->get('tab_design');
        $data['tab_image'] = $this->language->get('tab_image');

        if (isset($this->error['warning'])) {
            $data['error_warning'] = $this->error['warning'];
        } else {
            $data['error_warning'] = '';
        }

        if (isset($this->error['name'])) {
            $data['error_name'] = $this->error['name'];
        } else {
            $data['error_name'] = array();
        }

        if (isset($this->error['meta_title'])) {
            $data['error_meta_title'] = $this->error['meta_title'];
        } else {
            $data['error_meta_title'] = array();
        }

        if (isset($this->error['keyword'])) {
            $data['error_keyword'] = $this->error['keyword'];
        } else {
            $data['error_keyword'] = '';
        }

        $url = '';

        if (isset($this->request->get['sort'])) {
            $url .= '&sort=' . $this->request->get['sort'];
        }

        if (isset($this->request->get['order'])) {
            $url .= '&order=' . $this->request->get['order'];
        }

        if (isset($this->request->get['page'])) {
            $url .= '&page=' . $this->request->get['page'];
        }

        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title'),
            'href' => $this->url->link('catalog/pages', 'token=' . $this->session->data['token'] . $url, true)
        );

        if (!isset($this->request->get['pages_id'])) {
            $data['action'] = $this->url->link('catalog/pages/add', 'token=' . $this->session->data['token'] . $url, true);
        } else {
            $data['action'] = $this->url->link('catalog/pages/edit', 'token=' . $this->session->data['token'] . '&pages_id=' . $this->request->get['pages_id'] . $url, true);
        }

        $data['cancel'] = $this->url->link('catalog/pages', 'token=' . $this->session->data['token'] . $url, true);

        if (isset($this->request->get['pages_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
            $pages_info = $this->model_catalog_pages->getPages($this->request->get['pages_id']);
        }

        $data['token'] = $this->session->data['token'];

        $this->load->model('localisation/language');

        $data['languages'] = $this->model_localisation_language->getLanguages();

        if (isset($this->request->post['pages_description'])) {
            $data['pages_description'] = $this->request->post['pages_description'];
        } elseif (isset($this->request->get['pages_id'])) {
            $data['pages_description'] = $this->model_catalog_pages->getPagesDescriptions($this->request->get['pages_id']);
        } else {
            $data['pages_description'] = array();
        }

        if (isset($this->request->post['path'])) {
            $data['path'] = $this->request->post['path'];
        } elseif (!empty($pages_info)) {
            $data['path'] = $pages_info['path'];
        } else {
            $data['path'] = '';
        }

        if (isset($this->request->post['parent_id'])) {
            $data['parent_id'] = $this->request->post['parent_id'];
        } elseif (!empty($pages_info)) {
            $data['parent_id'] = $pages_info['parent_id'];
        } else {
            $data['parent_id'] = 0;
        }

        $this->load->model('catalog/filter');

        if (isset($this->request->post['pages_filter'])) {
            $filters = $this->request->post['pages_filter'];
        } elseif (isset($this->request->get['pages_id'])) {
            $filters = $this->model_catalog_pages->getPagesFilters($this->request->get['pages_id']);
        } else {
            $filters = array();
        }

        $data['pages_filters'] = array();

        foreach ($filters as $filter_id) {
            $filter_info = $this->model_catalog_filter->getFilter($filter_id);

            if ($filter_info) {
                $data['pages_filters'][] = array(
                    'filter_id' => $filter_info['filter_id'],
                    'name' => $filter_info['group'] . ' &gt; ' . $filter_info['name']
                );
            }
        }

        $this->load->model('setting/store');

        $data['stores'] = $this->model_setting_store->getStores();

        if (isset($this->request->post['pages_store'])) {
            $data['pages_store'] = $this->request->post['pages_store'];
        } elseif (isset($this->request->get['pages_id'])) {
            $data['pages_store'] = $this->model_catalog_pages->getPagesStores($this->request->get['pages_id']);
        } else {
            $data['pages_store'] = array(0);
        }

        if (isset($this->request->post['keyword'])) {
            $data['keyword'] = $this->request->post['keyword'];
        } elseif (!empty($pages_info)) {
            $data['keyword'] = $pages_info['keyword'];
        } else {
            $data['keyword'] = '';
        }

        if (isset($this->request->post['image'])) {
            $data['image'] = $this->request->post['image'];
        } elseif (!empty($pages_info)) {
            $data['image'] = $pages_info['image'];
        } else {
            $data['image'] = '';
        }

        $this->load->model('tool/image');

        if (isset($this->request->post['image']) && is_file(DIR_IMAGE . $this->request->post['image'])) {
            $data['thumb'] = $this->model_tool_image->resize($this->request->post['image'], 100, 100);
        } elseif (!empty($pages_info) && is_file(DIR_IMAGE . $pages_info['image'])) {
            $data['thumb'] = $this->model_tool_image->resize($pages_info['image'], 100, 100);
        } else {
            $data['thumb'] = $this->model_tool_image->resize('no_image.png', 100, 100);
        }

        $data['placeholder'] = $this->model_tool_image->resize('no_image.png', 100, 100);

        // Images
        if (isset($this->request->post['pages_image'])) {
            $pages_images = $this->request->post['pages_image'];
        } elseif (isset($this->request->get['pages_id'])) {
            $pages_images = $this->model_catalog_pages->getPagesImages($this->request->get['pages_id']);

        } else {
            $pages_images = array();
        }

        $data['pages_images'] = array();

        foreach ($pages_images as $pages_image) {
            if (is_file(DIR_IMAGE . $pages_image['image'])) {
                $image = $pages_image['image'];
                $thumb = $pages_image['image'];
            } else {
                $image = '';
                $thumb = 'no_image.png';
            }

            $data['pages_images'][] = array(
                'image' => $image,
                'thumb' => $this->model_tool_image->resize($thumb, 100, 100),
                'caption' => $pages_image['caption'],
                'sort_order' => $pages_image['sort_order']
            );
        }

        if (isset($this->request->post['top'])) {
            $data['top'] = $this->request->post['top'];
        } elseif (!empty($pages_info)) {
            $data['top'] = $pages_info['top'];
        } else {
            $data['top'] = 0;
        }

        if (isset($this->request->post['home'])) {
            $data['home'] = $this->request->post['home'];
        } elseif (!empty($pages_info)) {
            $data['home'] = $pages_info['home'];
        } else {
            $data['home'] = 0;
        }

        if (isset($this->request->post['using_breadcrumbs'])) {
            $data['using_breadcrumbs'] = $this->request->post['using_breadcrumbs'];
        } elseif (!empty($pages_info)) {
            $data['using_breadcrumbs'] = $pages_info['using_breadcrumbs'];
        } else {
            $data['using_breadcrumbs'] = 0;
        }

        if (isset($this->request->post['full_page'])) {
            $data['full_page'] = $this->request->post['full_page'];
        } elseif (!empty($pages_info)) {
            $data['full_page'] = $pages_info['full_page'];
        } else {
            $data['full_page'] = 0;
        }

        if (isset($this->request->post['column'])) {
            $data['column'] = $this->request->post['column'];
        } elseif (!empty($pages_info)) {
            $data['column'] = $pages_info['column'];
        } else {
            $data['column'] = 1;
        }

        if (isset($this->request->post['sort_order'])) {
            $data['sort_order'] = $this->request->post['sort_order'];
        } elseif (!empty($pages_info)) {
            $data['sort_order'] = $pages_info['sort_order'];
        } else {
            $data['sort_order'] = 0;
        }

        if (isset($this->request->post['status'])) {
            $data['status'] = $this->request->post['status'];
        } elseif (!empty($pages_info)) {
            $data['status'] = $pages_info['status'];
        } else {
            $data['status'] = true;
        }

        if (isset($this->request->post['pages_layout'])) {
            $data['pages_layout'] = $this->request->post['pages_layout'];
        } elseif (isset($this->request->get['pages_id'])) {
            $data['pages_layout'] = $this->model_catalog_pages->getPagesLayouts($this->request->get['pages_id']);
        } else {
            $data['pages_layout'] = array();
        }

        $this->load->model('design/layout');

        $data['layouts'] = $this->model_design_layout->getLayouts();

        $data['header'] = $this->load->controller('common/header');
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['footer'] = $this->load->controller('common/footer');

        $this->response->setOutput($this->load->view('catalog/pages_form', $data));
    }

    protected function validateForm()
    {
        if (!$this->user->hasPermission('modify', 'catalog/pages')) {
            $this->error['warning'] = $this->language->get('error_permission');
        }

        foreach ($this->request->post['pages_description'] as $language_id => $value) {
            if ((utf8_strlen($value['name']) < 2) || (utf8_strlen($value['name']) > 255)) {
                $this->error['name'][$language_id] = $this->language->get('error_name');
            }

            if ((utf8_strlen($value['meta_title']) < 3) || (utf8_strlen($value['meta_title']) > 255)) {
                $this->error['meta_title'][$language_id] = $this->language->get('error_meta_title');
            }
        }

        if (utf8_strlen($this->request->post['keyword']) > 0) {
            $this->load->model('catalog/url_alias');

            $url_alias_info = $this->model_catalog_url_alias->getUrlAlias($this->request->post['keyword']);

            if ($url_alias_info && isset($this->request->get['pages_id']) && $url_alias_info['query'] != 'pages_id=' . $this->request->get['pages_id']) {
                $this->error['keyword'] = sprintf($this->language->get('error_keyword'));
            }

            if ($url_alias_info && !isset($this->request->get['pages_id'])) {
                $this->error['keyword'] = sprintf($this->language->get('error_keyword'));
            }
        }

        if ($this->error && !isset($this->error['warning'])) {
            $this->error['warning'] = $this->language->get('error_warning');
        }

        return !$this->error;
    }

    protected function validateDelete()
    {
        if (!$this->user->hasPermission('modify', 'catalog/pages')) {
            $this->error['warning'] = $this->language->get('error_permission');
        }

        return !$this->error;
    }

    protected function validateRepair()
    {
        if (!$this->user->hasPermission('modify', 'catalog/pages')) {
            $this->error['warning'] = $this->language->get('error_permission');
        }

        return !$this->error;
    }

    public function autocomplete()
    {
        $json = array();

        if (isset($this->request->get['filter_name'])) {
            $this->load->model('catalog/pages');

            $filter_data = array(
                'filter_name' => $this->request->get['filter_name'],
                'sort' => 'name',
                'order' => 'ASC',
                'start' => 0,
                'limit' => 5
            );

            $results = $this->model_catalog_pages->getPagess($filter_data);

            foreach ($results as $result) {
                $json[] = array(
                    'pages_id' => $result['pages_id'],
                    'name' => strip_tags(html_entity_decode($result['name'], ENT_QUOTES, 'UTF-8'))
                );
            }
        }

        $sort_order = array();

        foreach ($json as $key => $value) {
            $sort_order[$key] = $value['name'];
        }

        array_multisort($sort_order, SORT_ASC, $json);

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }
}
