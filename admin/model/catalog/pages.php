<?php
class ModelCatalogPages extends Model {
	public function addPages($data) {
		$this->db->query("INSERT INTO " . DB_PREFIX . "pages SET parent_id = '" . (int)$data['parent_id'] . "', `top` = '" . (isset($data['top']) ? (int)$data['top'] : 0) . "', `home` = '" . (isset($data['home']) ? (int)$data['home'] : 0) . "', `using_breadcrumbs` = '" . (isset($data['using_breadcrumbs']) ? (int)$data['using_breadcrumbs'] : 0) . "', `full_page` = '" . (isset($data['full_page']) ? (int)$data['full_page'] : 0) . "', `column` = '" . (int)$data['column'] . "', sort_order = '" . (int)$data['sort_order'] . "', status = '" . (int)$data['status'] . "', date_modified = NOW(), date_added = NOW()");

		$pages_id = $this->db->getLastId();

		if (isset($data['image'])) {
			$this->db->query("UPDATE " . DB_PREFIX . "pages SET image = '" . $this->db->escape($data['image']) . "' WHERE pages_id = '" . (int)$pages_id . "'");
		}

		foreach ($data['pages_description'] as $language_id => $value) {
			$this->db->query("INSERT INTO " . DB_PREFIX . "pages_description SET pages_id = '" . (int)$pages_id . "', language_id = '" . (int)$language_id . "', name = '" . $this->db->escape($value['name']) . "', sub_title = '" . $this->db->escape($value['sub_title']) . "', description_small = '" . $this->db->escape($value['description_small']) . "', description = '" . $this->db->escape($value['description']) . "', meta_title = '" . $this->db->escape($value['meta_title']) . "', meta_description = '" . $this->db->escape($value['meta_description']) . "', meta_keyword = '" . $this->db->escape($value['meta_keyword']) . "'");
		}

		// MySQL Hierarchical Data Closure Table Pattern
		$level = 0;

		$query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "pages_path` WHERE pages_id = '" . (int)$data['parent_id'] . "' ORDER BY `level` ASC");

		foreach ($query->rows as $result) {
			$this->db->query("INSERT INTO `" . DB_PREFIX . "pages_path` SET `pages_id` = '" . (int)$pages_id . "', `path_id` = '" . (int)$result['path_id'] . "', `level` = '" . (int)$level . "'");

			$level++;
		}

		$this->db->query("INSERT INTO `" . DB_PREFIX . "pages_path` SET `pages_id` = '" . (int)$pages_id . "', `path_id` = '" . (int)$pages_id . "', `level` = '" . (int)$level . "'");

		if (isset($data['pages_filter'])) {
			foreach ($data['pages_filter'] as $filter_id) {
				$this->db->query("INSERT INTO " . DB_PREFIX . "pages_filter SET pages_id = '" . (int)$pages_id . "', filter_id = '" . (int)$filter_id . "'");
			}
		}

		if (isset($data['pages_store'])) {
			foreach ($data['pages_store'] as $store_id) {
				$this->db->query("INSERT INTO " . DB_PREFIX . "pages_to_store SET pages_id = '" . (int)$pages_id . "', store_id = '" . (int)$store_id . "'");
			}
		}

		// Set which layout to use with this pages
		if (isset($data['pages_layout'])) {
			foreach ($data['pages_layout'] as $store_id => $layout_id) {
				$this->db->query("INSERT INTO " . DB_PREFIX . "pages_to_layout SET pages_id = '" . (int)$pages_id . "', store_id = '" . (int)$store_id . "', layout_id = '" . (int)$layout_id . "'");
			}
		}

		if (isset($data['pages_image'])) {
			foreach ($data['pages_image'] as $pages_image) {
				$this->db->query("INSERT INTO " . DB_PREFIX . "pages_image SET pages_id = '" . (int)$pages_id . "', image = '" . $this->db->escape($pages_image['image']) . "', caption = '" . $this->db->escape($pages_image['caption']) . "', sort_order = '" . (int)$pages_image['sort_order'] . "'");
			}
		}

		if (isset($data['keyword'])) {
			$this->db->query("INSERT INTO " . DB_PREFIX . "url_alias SET query = 'pages_id=" . (int)$pages_id . "', keyword = '" . $this->db->escape($data['keyword']) . "'");
		}

		$this->cache->delete('pages');

		return $pages_id;
	}

	public function editPages($pages_id, $data) {
		$this->db->query("UPDATE " . DB_PREFIX . "pages SET parent_id = '" . (int)$data['parent_id'] . "', `top` = '" . (isset($data['top']) ? (int)$data['top'] : 0) . "', `home` = '" . (isset($data['home']) ? (int)$data['home'] : 0) . "',`using_breadcrumbs` = '" . (isset($data['using_breadcrumbs']) ? (int)$data['using_breadcrumbs'] : 0) . "', `full_page` = '" . (isset($data['full_page']) ? (int)$data['full_page'] : 0) . "', `column` = '" . (int)$data['column'] . "', sort_order = '" . (int)$data['sort_order'] . "', status = '" . (int)$data['status'] . "', date_modified = NOW() WHERE pages_id = '" . (int)$pages_id . "'");

		if (isset($data['image'])) {
			$this->db->query("UPDATE " . DB_PREFIX . "pages SET image = '" . $this->db->escape($data['image']) . "' WHERE pages_id = '" . (int)$pages_id . "'");
		}

		$this->db->query("DELETE FROM " . DB_PREFIX . "pages_description WHERE pages_id = '" . (int)$pages_id . "'");

		foreach ($data['pages_description'] as $language_id => $value) {
            $this->db->query("INSERT INTO " . DB_PREFIX . "pages_description SET pages_id = '" . (int)$pages_id . "', language_id = '" . (int)$language_id . "', name = '" . $this->db->escape($value['name']) . "', sub_title = '" . $this->db->escape($value['sub_title']) . "', description_small = '" . $this->db->escape($value['description_small']) . "', description = '" . $this->db->escape($value['description']) . "', meta_title = '" . $this->db->escape($value['meta_title']) . "', meta_description = '" . $this->db->escape($value['meta_description']) . "', meta_keyword = '" . $this->db->escape($value['meta_keyword']) . "'");
		}

		// MySQL Hierarchical Data Closure Table Pattern
		$query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "pages_path` WHERE path_id = '" . (int)$pages_id . "' ORDER BY level ASC");

		if ($query->rows) {
			foreach ($query->rows as $pages_path) {
				// Delete the path below the current one
				$this->db->query("DELETE FROM `" . DB_PREFIX . "pages_path` WHERE pages_id = '" . (int)$pages_path['pages_id'] . "' AND level < '" . (int)$pages_path['level'] . "'");

				$path = array();

				// Get the nodes new parents
				$query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "pages_path` WHERE pages_id = '" . (int)$data['parent_id'] . "' ORDER BY level ASC");

				foreach ($query->rows as $result) {
					$path[] = $result['path_id'];
				}

				// Get whats left of the nodes current path
				$query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "pages_path` WHERE pages_id = '" . (int)$pages_path['pages_id'] . "' ORDER BY level ASC");

				foreach ($query->rows as $result) {
					$path[] = $result['path_id'];
				}

				// Combine the paths with a new level
				$level = 0;

				foreach ($path as $path_id) {
					$this->db->query("REPLACE INTO `" . DB_PREFIX . "pages_path` SET pages_id = '" . (int)$pages_path['pages_id'] . "', `path_id` = '" . (int)$path_id . "', level = '" . (int)$level . "'");

					$level++;
				}
			}
		} else {
			// Delete the path below the current one
			$this->db->query("DELETE FROM `" . DB_PREFIX . "pages_path` WHERE pages_id = '" . (int)$pages_id . "'");

			// Fix for records with no paths
			$level = 0;

			$query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "pages_path` WHERE pages_id = '" . (int)$data['parent_id'] . "' ORDER BY level ASC");

			foreach ($query->rows as $result) {
				$this->db->query("INSERT INTO `" . DB_PREFIX . "pages_path` SET pages_id = '" . (int)$pages_id . "', `path_id` = '" . (int)$result['path_id'] . "', level = '" . (int)$level . "'");

				$level++;
			}

			$this->db->query("REPLACE INTO `" . DB_PREFIX . "pages_path` SET pages_id = '" . (int)$pages_id . "', `path_id` = '" . (int)$pages_id . "', level = '" . (int)$level . "'");
		}

		$this->db->query("DELETE FROM " . DB_PREFIX . "pages_filter WHERE pages_id = '" . (int)$pages_id . "'");

		if (isset($data['pages_filter'])) {
			foreach ($data['pages_filter'] as $filter_id) {
				$this->db->query("INSERT INTO " . DB_PREFIX . "pages_filter SET pages_id = '" . (int)$pages_id . "', filter_id = '" . (int)$filter_id . "'");
			}
		}

		$this->db->query("DELETE FROM " . DB_PREFIX . "pages_to_store WHERE pages_id = '" . (int)$pages_id . "'");

		if (isset($data['pages_store'])) {
			foreach ($data['pages_store'] as $store_id) {
				$this->db->query("INSERT INTO " . DB_PREFIX . "pages_to_store SET pages_id = '" . (int)$pages_id . "', store_id = '" . (int)$store_id . "'");
			}
		}

		$this->db->query("DELETE FROM " . DB_PREFIX . "pages_to_layout WHERE pages_id = '" . (int)$pages_id . "'");

		if (isset($data['pages_layout'])) {
			foreach ($data['pages_layout'] as $store_id => $layout_id) {
				$this->db->query("INSERT INTO " . DB_PREFIX . "pages_to_layout SET pages_id = '" . (int)$pages_id . "', store_id = '" . (int)$store_id . "', layout_id = '" . (int)$layout_id . "'");
			}
		}

        $this->db->query("DELETE FROM " . DB_PREFIX . "pages_image WHERE pages_id = '" . (int)$pages_id . "'");

        if (isset($data['pages_image'])) {
            foreach ($data['pages_image'] as $pages_image) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "pages_image SET pages_id = '" . (int)$pages_id . "', image = '" . $this->db->escape($pages_image['image']) . "', caption = '" . $this->db->escape($pages_image['caption']) . "', sort_order = '" . (int)$pages_image['sort_order'] . "'");
            }
        }

		$this->db->query("DELETE FROM " . DB_PREFIX . "url_alias WHERE query = 'pages_id=" . (int)$pages_id . "'");

		if ($data['keyword']) {
			$this->db->query("INSERT INTO " . DB_PREFIX . "url_alias SET query = 'pages_id=" . (int)$pages_id . "', keyword = '" . $this->db->escape($data['keyword']) . "'");
		}

		$this->cache->delete('pages');
	}

	public function deletePages($pages_id) {
		$this->db->query("DELETE FROM " . DB_PREFIX . "pages_path WHERE pages_id = '" . (int)$pages_id . "'");

		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "pages_path WHERE path_id = '" . (int)$pages_id . "'");

		foreach ($query->rows as $result) {
			$this->deletepages($result['pages_id']);
		}

		$this->db->query("DELETE FROM " . DB_PREFIX . "pages WHERE pages_id = '" . (int)$pages_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "pages_description WHERE pages_id = '" . (int)$pages_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "pages_filter WHERE pages_id = '" . (int)$pages_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "pages_to_store WHERE pages_id = '" . (int)$pages_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "pages_to_layout WHERE pages_id = '" . (int)$pages_id . "'");
		//$this->db->query("DELETE FROM " . DB_PREFIX . "product_to_pages WHERE pages_id = '" . (int)$pages_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "url_alias WHERE query = 'pages_id=" . (int)$pages_id . "'");
		//$this->db->query("DELETE FROM " . DB_PREFIX . "coupon_pages WHERE pages_id = '" . (int)$pages_id . "'");

		$this->cache->delete('pages');
	}

	public function repairPagess($parent_id = 0) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "pages WHERE parent_id = '" . (int)$parent_id . "'");

		foreach ($query->rows as $pages) {
			// Delete the path below the current one
			$this->db->query("DELETE FROM `" . DB_PREFIX . "pages_path` WHERE pages_id = '" . (int)$pages['pages_id'] . "'");

			// Fix for records with no paths
			$level = 0;

			$query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "pages_path` WHERE pages_id = '" . (int)$parent_id . "' ORDER BY level ASC");

			foreach ($query->rows as $result) {
				$this->db->query("INSERT INTO `" . DB_PREFIX . "pages_path` SET pages_id = '" . (int)$pages['pages_id'] . "', `path_id` = '" . (int)$result['path_id'] . "', level = '" . (int)$level . "'");

				$level++;
			}

			$this->db->query("REPLACE INTO `" . DB_PREFIX . "pages_path` SET pages_id = '" . (int)$pages['pages_id'] . "', `path_id` = '" . (int)$pages['pages_id'] . "', level = '" . (int)$level . "'");

			$this->repairPagess($pages['pages_id']);
		}
	}

	public function getPages($pages_id) {
		$query = $this->db->query("SELECT DISTINCT *, (SELECT GROUP_CONCAT(cd1.name ORDER BY level SEPARATOR '&nbsp;&nbsp;&gt;&nbsp;&nbsp;') FROM " . DB_PREFIX . "pages_path cp LEFT JOIN " . DB_PREFIX . "pages_description cd1 ON (cp.path_id = cd1.pages_id AND cp.pages_id != cp.path_id) WHERE cp.pages_id = c.pages_id AND cd1.language_id = '" . (int)$this->config->get('config_language_id') . "' GROUP BY cp.pages_id) AS path, (SELECT DISTINCT keyword FROM " . DB_PREFIX . "url_alias WHERE query = 'pages_id=" . (int)$pages_id . "') AS keyword FROM " . DB_PREFIX . "pages c LEFT JOIN " . DB_PREFIX . "pages_description cd2 ON (c.pages_id = cd2.pages_id) WHERE c.pages_id = '" . (int)$pages_id . "' AND cd2.language_id = '" . (int)$this->config->get('config_language_id') . "'");

		return $query->row;
	}

	public function getPagess($data = array()) {
		$sql = "SELECT cp.pages_id AS pages_id, GROUP_CONCAT(cd1.name ORDER BY cp.level SEPARATOR '&nbsp;&nbsp;&gt;&nbsp;&nbsp;') AS name, c1.parent_id, c1.sort_order FROM " . DB_PREFIX . "pages_path cp LEFT JOIN " . DB_PREFIX . "pages c1 ON (cp.pages_id = c1.pages_id) LEFT JOIN " . DB_PREFIX . "pages c2 ON (cp.path_id = c2.pages_id) LEFT JOIN " . DB_PREFIX . "pages_description cd1 ON (cp.path_id = cd1.pages_id) LEFT JOIN " . DB_PREFIX . "pages_description cd2 ON (cp.pages_id = cd2.pages_id) WHERE cd1.language_id = '" . (int)$this->config->get('config_language_id') . "' AND cd2.language_id = '" . (int)$this->config->get('config_language_id') . "'";

		if (!empty($data['filter_name'])) {
			$sql .= " AND cd2.name LIKE '" . $this->db->escape($data['filter_name']) . "%'";
		}

		$sql .= " GROUP BY cp.pages_id";

		$sort_data = array(
			'name',
			'sort_order'
		);

		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];
		} else {
			$sql .= " ORDER BY sort_order";
		}

		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}

		$query = $this->db->query($sql);

		return $query->rows;
	}

	public function getPagesDescriptions($pages_id) {
		$pages_description_data = array();

		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "pages_description WHERE pages_id = '" . (int)$pages_id . "'");

		foreach ($query->rows as $result) {
			$pages_description_data[$result['language_id']] = array(
				'name'             => $result['name'],
				'meta_title'       => $result['meta_title'],
				'meta_description' => $result['meta_description'],
				'meta_keyword'     => $result['meta_keyword'],
				'description'      => $result['description'],
				'description_small'      => $result['description_small'],
                'sub_title' => $result['sub_title'],
                'description_small' => $result['description_small']
			);
		}

		return $pages_description_data;
	}

	public function getPagesFilters($pages_id) {
		$pages_filter_data = array();

		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "pages_filter WHERE pages_id = '" . (int)$pages_id . "'");

		foreach ($query->rows as $result) {
			$pages_filter_data[] = $result['filter_id'];
		}

		return $pages_filter_data;
	}

	public function getPagesStores($pages_id) {
		$pages_store_data = array();

		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "pages_to_store WHERE pages_id = '" . (int)$pages_id . "'");

		foreach ($query->rows as $result) {
			$pages_store_data[] = $result['store_id'];
		}

		return $pages_store_data;
	}

	public function getPagesLayouts($pages_id) {
		$pages_layout_data = array();

		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "pages_to_layout WHERE pages_id = '" . (int)$pages_id . "'");

		foreach ($query->rows as $result) {
			$pages_layout_data[$result['store_id']] = $result['layout_id'];
		}

		return $pages_layout_data;
	}

	public function getTotalPagess() {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "pages");

		return $query->row['total'];
	}
	
	public function getTotalPagessByLayoutId($layout_id) {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "pages_to_layout WHERE layout_id = '" . (int)$layout_id . "'");

		return $query->row['total'];
	}

    public function getPagesImages($pages_id)
    {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "pages_image WHERE pages_id = '" . (int)$pages_id . "' ORDER BY sort_order ASC");

        return $query->rows;
    }
}
