function instaFeedSlider(container, slider){
  
	var page_counter = 0;
	var page_limit = 4;
	var page_size = 60;
  
	var feed = new Instafeed({
		get: 'user',
		userId: 181462680,
		accessToken: '1403540713.5b9e1e6.f0d42f00cf10471990fad59a90a907ac',
		link: 'true',
		clientId: 'd036e6d5bf1b477e8d8bfa0df8a79864',
		limit: page_size,
		template: '<div class="i"><a href="{{link}}" target="_blank"><div class="overlay"><div class="caption">{{caption}}</div></div><img src="{{image}}" /></a></a></div>',
		resolution: 'low_resolution',
		filter: function(image) {
			var foundTag = false, tags = image.tags;
 			for(var i=0,len=tags.length; i<len && !foundTag; i++) {

				if (tags[i] === 'worthholdingonto') foundTag = true;
			};
 
			return foundTag;
		},
		after: function(){
    		if(page_counter++ <= page_limit){
              feed.next();
            }else{
              check(container, slider);
            }
    
  		}
	});
	    
    
	feed.run();
  
//  check(container, slider);
  
}

function check(container, slider){

  	var i_ready = ($(container).children().size())>0;
    
  	if(i_ready) return(instaFeedsLoaded(container, slider));
  
	setTimeout(function(){check(container, slider)},100);
}



function instaFeedsLoaded(container, slider){
    
		var a = [];
      
		$(container).find('div.i').each(
	
			function(i,o){
				a.push($(o));
			}
	
		);
      

  
	for(i=0; i<a.length; i+=8){
      
		var $li = $('<li>')
		
        for(j=i; j<=i+7; j++){
        
			var $o = a[j];
			$li.append($o);
        
		}
      
		$(slider).find('ul').append($li);
      
	}
      
//	$(container).remove();
      

	$(slider).flexslider({
        slideshowSpeed: 70000,
        animation:'slide'
    });
      
}