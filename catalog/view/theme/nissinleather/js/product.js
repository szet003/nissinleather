$(document).ready(function(){
  (function() {
    var $gallery = $('#gallery');
    var $holder = $('.holder', $gallery);
    var $carousel = $('.carousel', $gallery);

    $holder.flexslider({
      animation: "fade",
      controlNav: false,
      animationLoop: false,
      slideshow: false,
      directionNav: false,
      start: function() {
        if ($holder.innerHeight() > 100) {
          $carousel.css({'height': $holder.innerHeight()});
        }
      }
    });

    $('.placeholder', $gallery).css('position', 'absolute');

    $('#product-summary').find('.size-and-fit').each(function() {
      var $this = $(this);
      var $table = $('table', $this).hide();
      var $link = $('a.size-and-fit-link', $this);
      if ($link.length == 0) {
        $link = $('<a href="#">View product measurements</a>').insertBefore($table);
      }

      $link.click(function(event) {
        event.preventDefault();
        var width = 700;
        var height = 400;
        var left = (window.screen.width - width) / 2;
        var top = (window.screen.height - height) / 2;
        var url = $this.data('size-fit');

        window.open(location.origin+url, '', 'width='+width+',height='+height+',resizeable,scrollbars,top='+top+',left='+left);
      });
    });

    $(window).resize(function() {
      $carousel.css({'height': $holder.innerHeight()});
    });

    // init carousel
    var numberOfItems = $('.thumbnail', $carousel).length;

    $carousel.on('touchstart click', '.thumbnail', function() {
      $holder.flexslider($(this).data('slide'));
    });

    if (numberOfItems > 5) {
      var itemHeight = $('.thumbnail', $carousel).first().outerHeight(true);
      var limit = (numberOfItems - 4) * itemHeight;

      $('.next', $carousel).show();
      $carousel.on('touchstart click', '.prev', function(event) {
        event.preventDefault();
        var currentPage = $carousel.data('current-page') - 1;
        var offset = currentPage * itemHeight * 4;

        $carousel.data('current-page', currentPage);

        $('ul', $carousel).css({
          '-webkit-transition-duration': '0.6s',
          'transition-duration': '0.6s',
          '-webkit-transform': 'translate3d(0, -' + offset + 'px, 0)',
          'transform': 'translate3d(0, -' + offset + 'px, 0)'
        });

        if (currentPage == 0) {
          $('.prev', $carousel).hide();
        }

        $('.next', $carousel).show();
      });

      $carousel.on('click', '.next', function(event) {
        event.preventDefault();
        var currentPage = $carousel.data('current-page') + 1;
        var offset = currentPage * itemHeight * 4 > limit ? limit : currentPage * itemHeight * 4;

        $carousel.data('current-page', currentPage);

        $('ul', $carousel).css({
          '-webkit-transition-duration': '0.6s',
          'transition-duration': '0.6s',
          '-webkit-transform': 'translate3d(0, -' + offset + 'px, 0)',
          'transform': 'translate3d(0, -' + offset + 'px, 0)'
        });

        if (offset == limit) {
          $('.next', $carousel).hide();
        }

        $('.prev', $carousel).show();
      });
    }
  })();
});