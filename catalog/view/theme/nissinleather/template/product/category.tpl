<?php echo $header; ?>
<section class="main-content">
    <header class="row" style="margin-bottom: -90px;">
        <div class="left columns large-6">


            <ul class="breadcrumbs colored-links abel14" style="padding-bottom: 0px;">
                <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
                <?php } ?>

            </ul>

        </div>
        <div class="right columns large-6" style="margin-bottom: 0px;">
            <div class="tag-filter">
                <span class="title abel14" style="margin-top: 5px;">Sort by Style |</span>
                <form class="custom">
                    <select class="small tags-listbox" name="coll-filter" size="1">
                        <option value="">All</option>
                        <?php foreach ($filter_groups as $filter_group) { ?>
                        <?php foreach ($filter_group['filter'] as $filter) { ?>
                            <?php if (in_array($filter['filter_id'], $filter_category)) { ?>
                                <option value="<?php echo $filter['filter_id']; ?>" selected="selected"><?php echo $filter['name']; ?></option>
                            <?php } else { ?>
                                <option value="<?php echo $filter['filter_id']; ?>"><?php echo $filter['name']; ?></option>
                            <?php } ?>
                        <?php } ?>
                        <?php } ?>
                    </select>
                </form>
            </div>
        </div>
    </header>

    <div class="row">
        <?php echo $content_top; ?>

        <section class="row">
            <?php if ($products) { ?>
            <div class="product-grid clearfix">
                <div class="clearfix"></div>
                <?php foreach ($products as $product) { ?>
                <div class="product-item columns large-3" style="padding:0 1% 0 1%;">
                    <a href="<?php echo $product['href']; ?>">
                        <div class="image-wrapper trn2">
                            <img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>"
                                 title="<?php echo $product['name']; ?>"/>
                        </div><!-- .coll-image-wrap -->
                        <div class="caption">
                            <p class="vendorTitle">Tanner Goods</p>
                            <div class="title"><p class="trn2"><?php echo $product['name']; ?></p>
                                <p class="trn2"><?php echo $product['model']; ?></p></div>
                            <div class="price"><span class='money'><?php echo $product['price']; ?></span></div>
                        </div><!-- .coll-prod-caption -->
                    </a>
                </div>
                <?php } ?>
            </div>
            <?php } ?>
        </section>
    </div>
    <footer class="row">
    <div class="right columns large-6">
        <?php echo $pagination; ?>
    </div>

    </footer>


    <script>
        /* Collection Easing */



        /* Product Tag Filters - Good for any number of filters on any type of collection pages */
        /* Brought to you by Caroline Schnapp */
        var collFilters = jQuery('.tags-listbox');
        collFilters.change(function () {
            var newTags = [];
            collFilters.each(function () {
                if (jQuery(this).val()) {
                    newTags.push(jQuery(this).val());
                }
            });
            if (newTags.length) {
                var query = newTags.join('+');
                window.location.href = jQuery('<a title="Show products matching tag tag" href="<?php echo $current ?>?filter=tag">tag</a>').attr('href').replace('tag', query);
            }
            else {

                window.location.href = '<?php echo $current ?>';

            }
        });

        var elementArray = [];
        var length = ($('.product-item').length);
        var counter = 0;
        var elements = $('.product-item');
        function handleShowOnScroll() {
            $(elements).each(function (index, el) {
                var $el = $(el);
                var bottom = $(window).scrollTop() + $(window).height();
                var elTop = $el.offset().top;
                if (elTop < (bottom - 100)) {
                    elementArray.push($el)
                    elements.splice(0, 1);
                }
            })
        }
        ;


        var revealItems = function () {
            if (counter < length) {
                if (elementArray[0]) {
                    elementArray[0].animate({'opacity':'1'}, 500);
                    elementArray.shift();
                    counter++;
                }
            }
            else if (counter === length) {
                clearInterval(revealInterval);
            }
        };


        var revealInterval = setInterval(function () {
            revealItems()
        }, 100);


        $(window).on('scroll', handleShowOnScroll);
        handleShowOnScroll();
        revealInterval();


    </script>
    <script type="text/javascript" charset="utf-8">
        //

        var products_count = <?php echo $product_count; ?>,
            limit = <?php echo $limit; ?>,
                pages_count = Math.ceil(products_count / limit),
//                base_url = location.pathname + "?page=",
                i = 2;
        if (pages_count > 1) {
            $('#pagination').hide();

            var getItems = function () {
                var dfd = $.Deferred(),
                        collection = $('#three-column-collectionlist');

                return dfd.promise();
            }

            getItems().done(function () {
                setLazy();
                $('#pagination').hide();
                window.scroll(0, 1);
            });
        }

    </script>
</section>


<div class="footerpad"></div>
<?php echo $footer; ?>
