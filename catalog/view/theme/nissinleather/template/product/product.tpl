<?php echo $header; ?>
<section class="main-content">
    <header>
        <div class="row show-for-medium-up">
            <div class="columns">

                <ul class="breadcrumbs colored-links abel14" style="padding-bottom: 0px;">
                    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
                    <?php } ?>

                </ul>

            </div>
        </div>
    </header>
    <article class="row" itemscope itemtype="<?php echo $current; ?>">
        <meta itemprop="url" content="<?php echo $current; ?>"/>
        <?php if ($thumb) { ?>
        <meta itemprop="image"
              content="<?php echo $popup; ?>"/>
        <?php } ?>


        <div class="row">
            <div class="columns">

                <!--images-->

                <div class="p_images">
                    <?php if ($images) { ?>

                    <?php if ($images) { ?>
                    <?php foreach ($images as $image) { ?>
                    <img src="<?php echo $image['thumb']; ?>" title="<?php echo $heading_title; ?>"
                         alt="<?php echo $heading_title; ?>"/>
                    <?php } ?>
                    <?php } ?>

                    <?php } ?>

                    <div class="pager"></div>


                </div><!--p_images-->
                <div class="p_copy">
                    <form class="custom cart-form" action="/cart/add" method="post" enctype="multipart/form-data">

                        <div class="pcpytitle">
                            <h3><?php echo $category; ?></h3>
                            <h1><?php echo $heading_title; ?></h1>
                            <h2><?php echo $model; ?></h2> <!-- <h2> / One Size </h2>  -->


                            <div class="prices" itemprop="offers" itemscope itemtype="http://schema.org/Offer">
                                <meta itemprop="priceCurrency" content="USD"/>

                                <link itemprop="availability" href="http://schema.org/InStock"/>

                                <p>

                                    <span class="actual-price" itemprop="price"><span
                                                class='money'><?php echo $price; ?></span></span>

                                </p>
                            </div>


                        </div>

                        <ul class="product_accord">

                            <li>
                                <h3>Description</h3>
                                <?php if($description) { ?>
                                <section>
                                    <?php echo $description; ?>
                                </section>
                                <?php } ?>
                            </li>

                            <li>
                                <h3>Details</h3>
                                <?php if($details) { ?>
                                <section>
                                    <?php echo $details; ?>
                                </section>
                                <?php } ?>
                            </li>
                            <li>
                                <h3>Shipping Details</h3>
                                <?php if($shipping_details) { ?>
                                <section>
                                    <?php echo $shipping_details; ?>
                                </section>
                                <?php } ?>
                            </li>
                        </ul>


                        <!-- added by troop variant-->
                        <div class="variants hidden">

                            <select id="variant-listbox" name="id" class="medium">

                                <option value="16545474437">Navy Salt & Pepper / N/A / One Size - <span class='money'>$300.00</span>
                                </option>

                            </select>
                        </div>


                        <div class="quanity-cart-row clearfix">
                            <div class="quantity">
                                <label for="quantity">Quantity</label>
                                <input id="quantity" type="number" name="quantity" value="1"/>
                            </div>
                        </div>
                        <!--qty-->


                        <div class="buy_wrap">

                            <div class="add-to-cart">
                                <input type="submit" name="add" id="add" value="Add to cart" class="purchase button"
                                       style="width:100%">
                                <script data-product=""
                                        id="out-of-stock"
                                        src="//setup.shopapps.io/out-of-stock/script/tannergoods.js"></script>


                                <span class="sold-out long disabled">Call one of our <a
                                            href="http://www.tannergoods.com/pages/find-a-dealer">flagship stores</a> to check in-store availability.</span>

                            </div><!--add-to-cart-->

                            <p class="shipping_note">Questions about our products? Give us a call <a
                                        href="tel:1-503-862-3224" style="color:#c8b18b"><?php echo $phone; ?></a></p>


                        </div><!--buy_wrap-->


                    </form>
                </div><!--p_copy-->


            </div><!--columns-->
        </div><!--row-->

    </article>
    <!--hero-->

    <?php if($big_image) { ?>
    <div class="lng_img">
        <img src="<?php echo $big_image; ?>" alt=""/>
    </div><!--lng_img-->
    <?php } ?>


    <script>!window.jQuery && document.write('<script src="//ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"><\/script>')</script>

    <script type="text/javascript" charset="utf-8">
        //<![CDATA[

        var crosssellsettings = eval("[{ \"productpagemaxitems\" : \"4\",\"titleproductpage\" : \"<?php echo $text_related_products; ?>\",\"titleproductpagedefault\" : \"SIMILAR STYLES\",\"titlecheckoutpage\" : \"Before you checkout, have you considered...\",\"showdefault\" : \"2\",\"checkoutpagemaxitems\" : \"5\",\"showcheckout\" : \"1\",\"cstr\" : \"on\"}]");


        //product specific settings
        var crossselltitleproductpage = crosssellsettings[0]['titleproductpage'];
        var crossselltitleproductpagedefault = crosssellsettings[0]['titleproductpagedefault'];
        var crosssellmaxitems = crosssellsettings[0]['productpagemaxitems'];
        var crosssellshowdefault = crosssellsettings[0]['showdefault'];


        //]]>
        </script>

        <article id="cross-sell" class="similar_clear row" itemscope="" itemtype="http://schema.org/Product"></article>

        <script type="text/javascript" charset="utf-8"><!--

        var tmpdata = '';
        var tmptitle = '';
        //First of all, see if this product has any handpicked crosssell items
        if (crosssellshowdefault != '4') {
        //See if the selected product has any cross-sell items
        tmpdata = eval('<?php print_r($products);?>');
            if (crossselltitleproductpage.length) //if a title has been selected
            {
            tmptitle = crossselltitleproductpage;
            }
        }

            if(crosssellshowdefault == '1' || (crosssellshowdefault == '2' && tmpdata == ''))
                {
                  //Show default cross-sell items on all product pages? Override all
                  //See if any default cross-sell items have been selected for the product page

                }


              if(tmpdata)
              {
                jQuery('#cross-sell').append('<div class="row"><div class="columns"><h2 style="text-transform:uppercase;">' + tmptitle + '</h2><section class="similar-products"><div class="product-grid cross-sell"><div class="clearfix"></div></div></section></div></div>'); // write the title and container to the page
                var crosssell = [];
                var order = '';
            	var producthandle = 'voyager-daypack-navy-salt-and-pepper';

                //Set the maximum number of items that should be shown?
                var numproducts = (crosssellmaxitems < tmpdata.length) ? crosssellmaxitems : tmpdata.length;
                //objects tend to randomize the output, so put the handles into an array first
                for (var i=0; i<tmpdata.length; i++)
                {
                  order = (Number(tmpdata[i]['order']) - 1);
                  crosssell[order] = tmpdata[i]['handle'];
                }

                if (crosssell.length && crosssell[0] !== '')
                {
                  var list = jQuery('.cross-sell');
                  for (var j=0; j<numproducts; j++)
                  {
                    jQuery.ajax({
                    cache: true,
                    async: false,
                    dataType: "json",
                    error: function(response) {
                        if(response['status'] == '404')
                        {
                            if(tmpdata.length > numproducts)
                            {
                               numproducts++;
                            }
                        }

                    },
                    success: function(product) {
                        //console.log(product); return false;
            			// Now output the products now that the data has loaded.
            			if(product.handle != producthandle)
            			{
            		        if(product.images[0])
            		        {
            		        	var imagename = product.images[0];

            		        }
            		        else
            		        {
            			    	var csimage = 'https://cdn.shopify.com/s/images/admin/no-image-'+imagesize+'.gif';
            			    }

            			    var csprice = Shopify.formatMoney(product.price, "<span class='money'>${{amount}}</span>");

            				var xselloutput = '<div class="product-item columns large-3" style="padding-right:0px;">'
                                        + '<a href="' + product.url + '">'
                                          + '<div class="image-wrapper trn2">'
                                             + '<img  src="' + imagename + '" alt="' + product.title + '" />'
                                          + '</div>'
                                          + '<div class="caption">'
                                            + '<p class="vendorTitle">' + product.category + '</p>'
                                            + '<div class="title"><p class="trn2">' + product.title + '</p><p class="trn2">' + product.model + '</p></div>'
                                            + '<div class="price">' + product.price + '</div>'
                                          + '</div>'
                                        + '</a>'
                                      + '</div>';

            				list.append(xselloutput);
                     	}
            		 	else
            		 	{
                      		if(tmpdata.length > numproducts)
            		  		{
            		  		numproducts++;
                        	}
                      	}
                    },
                    url: 'index.php?route=product/product/get_product_related&product_id=' + crosssell[j]
                  });
                }
              }
             }
            //Cross-sell settings
            var settingsoutput = '';
            jQuery.each(crosssellsettings[0], function(key, value){
            	settingsoutput += '<input type="hidden" name="' + key + '" id="cs_' + key + '" value="' + value + '" />';
            });
            jQuery('#cross-sell').append(settingsoutput);

--></script>

    <div class="pfoot row show-for-medium-up"></div>

<script type="text/javascript"><!--
    jQuery(document).ready(function($){


        $('.single-slider').jRange({
          from: 25,
          to: 40,
          step: 1,
          scale: [25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40],
          format: '%s"',
          width: 1200,
          showLabels: true,
          onstatechange : function($val){
            if($val%2 == 0)
              $('.cal_val').html(parseInt($val)+2);
            else
              $('.cal_val').html(parseInt($val)+1);
          }
        });
        if(parseInt($('.single-slider').val())%2 == 0)
          $('.cal_val').html(parseInt($('.single-slider').val())+2);
        else
          $('.cal_val').html(parseInt($('.single-slider').val())+1);

      });

      var elementArray = [];
      var length =  ($('.product-item').length);
      var counter = 0;
      var elements = $('.product-item');
      function handleShowOnScroll() {
        $(elements).each(function(index, el) {
          var $el = $(el);
          var bottom = $(window).scrollTop() + $(window).height();
          var elTop = $el.offset().top;
          if (elTop < (bottom - 100)){
            elementArray.push($el)
            elements.splice(0,1);
          }
        })
      };



      var revealItems = function(){
        if(counter < length){
          if(elementArray[0]){
            elementArray[0].animate({'opacity':'1'}, 500);
            elementArray.shift();
            counter++;
          }
        }
        else if(counter === length){
          clearInterval(revealInterval);
        }
      };


      var revealInterval = setInterval(function(){
        revealItems()
      }, 100);


      $(window).on('scroll', handleShowOnScroll);
      handleShowOnScroll();
      revealInterval();

--></script>
</section>
<div class="footerpad"></div>
<?php echo $footer; ?>