



<footer class="main-footer ">

  <div class="row top" >




    <div class="hidemob leftmn large-3 columns social-follow abel12">
      <a href="https://www.facebook.com/TannerGoods" target="_blank"><i class="fa fa-facebook-square"></i></a>
      <a href="http://twitter.com/tannergoods" target="_blank"><i class="fa fa-twitter-square"></i></a>
      <a href="http://tannergoods.tumblr.com/" target="_blank"><i class="fa fa-tumblr-square"></i></a>
      <a href="http://www.pinterest.com/source/tannergoods.com/" target="_blank"><i class="fa fa-pinterest-square"></i></a>
    </div>

    <div class="columns menu-container abel12">

      <ul class="" style="list-style: none; display:inline-block;">
        <li class="" >
          <a class="" href="/pages/shipping-info">
            Shipping Info
          </a>
        </li>
      </ul>

      <ul class="" style="list-style: none; display:inline-block;">
        <li class="" >
          <a class="" href="/pages/returns-exchanges">
            Returns & Exchanges
          </a>
        </li>
      </ul>

      <ul class="" style="list-style: none; display:inline-block;">
        <li class="" >
          <a class="" href="/pages/terms-conditions">
            Site Terms
          </a>
        </li>
      </ul>

      <ul class="" style="list-style: none; display:inline-block;">
        <li class="" >
          <a class="" href="/pages/privacy-policy">
            Privacy
          </a>
        </li>
      </ul>

      <ul class="" style="list-style: none; display:inline-block;">
        <li class="" >
          <a class="" href="#">
            Opportunities
          </a>
        </li>
      </ul>

      <ul class="" style="list-style: none; display:inline-block;">
        <li class="" >
          <a class="" href="#">
            Press Inquiry
          </a>
        </li>
      </ul>


    </div>




    <div class="hidemob rgtmenu large-3 columns abel12">
      <ul>



        <li> <a href="//tannergoods.myatonce.com/" >Wholesale Log in</a></li>

      </ul>
    </div><!--large-->






  </div><!-- .row -->


  <!-- <div class="row bottom">
     <div class="column-1 large-4 columns">
       <h2 class="title">Store</h2>
       <div class="content">
         <p class="text">© 2014 Designed with love by <a href="http://www.troop.ws">Troop<a/>
<br />
<strong> <a href="https://themes.shopify.com/themes/blockshop/styles/deli"> Buy Theme on Shopify </a></strong></p>
       </div>

     </div>
     <div class="column-2 large-4 columns">
       <h2 class="title">Info</h2>

       <div class="content">


         <ul class="footer-nav plain-list" role="navigation">

           <li><a href="/search" title="Search">Search</a></li>

           <li><a href="/pages/ethos" title="About Us">About Us</a></li>

         </ul>

       </div>

     </div>


     <div class="column-3 large-4 columns">


           <h2 class="title">Subscribe</h2>


         <div class="content" id="mailing-list-module">

             <p>Signup to get the latest news...</p>



             <form method="post" action="/contact#contact_form" id="contact_form" class="contact-form" accept-charset="UTF-8"><input type="hidden" value="customer" name="form_type" /><input type="hidden" name="utf8" value="✓" />


           <input type="hidden" id="contact_tags" name="contact[tags]" value="prospect,newsletter" />
           <input type="hidden" id="newsletter-first-name" name="contact[first_name]" value="Subscriber" />
           <input type="hidden" id="newsletter-last-name" name="contact[last_name]" value="Newsletter" />

           <div class="row collapse">
             <div class="small-10 large-10 columns" style="padding-right: 10px;">
               <input type="email" placeholder="Your Email" name="contact[email]" />
             </div>
             <div class="small-2 large-2 columns">
               <input type="submit" class="button prefix" value="OK" name="subscribe" id="email-submit" />
             </div>
           </div>

           </form>



         <div class="social-follow">
           <a title="facebook" aria-hidden="true" target="_blank" class="glyph facebook" href="https://www.facebook.com/TannerGoods"></a>
           <a title="twitter" aria-hidden="true" target="_blank" class="glyph twitter" href="/twitter"></a>
           <a title="instagram" aria-hidden="true" target="_blank" class="glyph instagram" href="http://instagram.com/tannergoods"></a>

           <a title="tumblr" aria-hidden="true" target="_blank" class="glyph tumblr" href="/tumblr"></a>


           <a title="rss" aria-hidden="true" target="_blank" class="glyph feed" href="/rss"></a>
         </div>
       </div>
     </div>
   </div>

   <div class="row">
     <div class="columns">

     </div>
   </div> -->
</footer>

<script src="<?php echo $theme_path.'js/'?>plugins.js" type="text/javascript"></script>
<script src="<?php echo $theme_path.'js/'?>shop.js" type="text/javascript"></script>
<script src="<?php echo $theme_path.'js/'?>jquery_003.js" type="text/javascript"></script>
<script src="<?php echo $theme_path.'js/'?>jquery_002.js" type="text/javascript"></script>
<script src="<?php echo $theme_path.'js/'?>product.js" type="text/javascript"></script>



</body>
</html>