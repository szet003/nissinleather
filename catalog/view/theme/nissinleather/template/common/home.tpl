<?php echo $header; ?>
<section class="main-content">
    <?php echo $content_top; ?>
    <div class="modal video">
        <div class="bg"></div>
        <a aria-hidden="true" class="close glyph cross"></a>
        <div class="player columns large-8">
            <div class="flex-video">
            </div>
        </div>

        <div class="caption columns large-4">

        </div>
    </div>
    <div class="modal-mask"></div>

    <div class="widgets">
        <div class="small-promos">
            <div class="row">
                <?php foreach($pagess as $pages) { ?>
                <div class="columns large-6">
                    <div class="image-text-widget">
                       <a href="<?php echo $pages['href'];?>">

                            <img src="<?php echo $pages['image'];?>" alt="" />

                            <div class="caption">
                                <div class="bg"></div>
                                <div class="inner">
                                    <h1><?php echo $pages['name'];?></h1>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
                <?php } ?>


            </div>
        </div>








    </div> <!-- .widgets -->
</section>
<div class="footerpad" style="height:0px;"> </div>
<?php echo $footer; ?>