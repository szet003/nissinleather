<!DOCTYPE html>
<!--[if IE]><![endif]-->
<!--[if IE 8 ]>
<html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" class="ie8"><![endif]-->
<!--[if IE 9 ]>
<html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" class="ie9"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>">
<!--<![endif]-->
<head>
    <meta charset="UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?php echo $title; ?></title>
    <base href="<?php echo $base; ?>"/>
    <?php if ($description) { ?>
    <meta name="description" content="<?php echo $description; ?>"/>
    <?php } ?>
    <?php if ($keywords) { ?>
    <meta name="keywords" content="<?php echo $keywords; ?>"/>
    <?php } ?>
    <script src="catalog/view/javascript/jquery/jquery-2.1.1.min.js" type="text/javascript"></script>
    <?php foreach ($styles as $style) { ?>
    <link href="<?php echo $style['href']; ?>" type="text/css" rel="<?php echo $style['rel']; ?>"
          media="<?php echo $style['media']; ?>"/>
    <?php } ?>
    <?php foreach ($links as $link) { ?>
    <link href="<?php echo $link['href']; ?>" rel="<?php echo $link['rel']; ?>"/>
    <?php } ?>
    <?php foreach ($scripts as $script) { ?>
    <script src="<?php echo $script; ?>" type="text/javascript"></script>
    <?php } ?>
    <?php foreach ($analytics as $analytic) { ?>
    <?php echo $analytic; ?>
    <?php } ?>
    <script>
        WebFontConfig = {


            google: {families: ['Abel', 'Abel', 'Abel', 'Abel', 'Abel',]}

        };
    </script>
    <script src="//ajax.googleapis.com/ajax/libs/webfont/1.4.7/webfont.js"></script>

    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
    <link href="<?php echo $theme_path.'stylesheet/'?>style.css" rel="stylesheet" type="text/css" media="all"/>
    <link href="<?php echo $theme_path.'stylesheet/'?>flexslider.css" rel="stylesheet" type="text/css" media="all"/>
    <!--[if lt IE 8]>
    <script src="<?php echo $theme_path.'js/'?>json2.js" type="text/javascript"></script>
    <![endif]-->

    <!--[if lt IE 9]>
    <script src="//html5shim.googlecode.com/svn/trunk/html5.js" type="text/javascript"></script>
    <script src="<?php echo $theme_path.'js/'?>dd_roundies.js" type="text/javascript"></script>
    <script>
        DD_roundies.addRule('.circle', '45px');
    </script>
    <![endif]-->
    <script src="<?php echo $theme_path.'js/'?>custom.modernizr.js" type="text/javascript"></script>
    <script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js" type="text/javascript"></script>
    <script src="<?php echo $theme_path.'js/'?>api.jquery.js" type="text/javascript"></script>
    <script src="<?php echo $theme_path.'js/'?>option_selection.js" type="text/javascript"></script>
    <script src="https://malsup.github.io/min/jquery.cycle2.min.js "></script>
    <script src="https://malsup.github.io/min/jquery.cycle2.swipe.min.js "></script>
    <script src="https://malsup.github.io/min/jquery.cycle2.carousel.min.js"></script>
    <link href="<?php echo $theme_path.'stylesheet/'?>entypo.css" rel="stylesheet" type="text/css" media="all"/>
    <link href='//fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
    <script src="<?php echo $theme_path.'js/'?>jquery.flexslider.js" type="text/javascript"></script>
    <script src="<?php echo $theme_path.'js/'?>owl.carousel.js" type="text/javascript"></script>
    <script src="<?php echo $theme_path.'js/'?>jquery.backstretch.min.js" type="text/javascript"></script>
    <script>
        var twitterID = '',
                shop_url = 'http://www.nissinleather.com',
                home_slider_auto_enabled = true,
                home_slider_rotate_frequency = 10000,
                product_zoom_enabled = true,
                product_modal_enabled = true;
    </script>

    <!-- product page size slider -->
    <link href="<?php echo $theme_path.'stylesheet/'?>rangeSlider.css" rel="stylesheet" type="text/css" media="all"/>
    <script src="<?php echo $theme_path.'js/'?>rangeSlider.js" type="text/javascript"></script>


</head>
<body class="<?php echo $class; ?>">
<div class="b-infobar">
    <div class="b-infobar__text">
        Free shipping on all domestic orders $150+
    </div>
    <!-- /.b-infobar__text -->
</div>
<!-- /.b-infobar -->


<header class="main-header">
    <div class="bg" style="border-bottom: 1px solid #c8b18b;"></div>
    <div class="row top">


        <div class="hidemob leftmn large-3 columns social-follow abel14" style="margin-top:33px;">
            <a title="Newsletter" aria-hidden="true" class="newsPop" href="#"><i class="fa fa-envelope"></i> Newsletter
                Sign Up</a>


        </div>

        <div class="centernav columns menu-container" style="font-family: abel;font-size:18px; margin-top:20px;">

            <div class="hidetabletleft">
                <div class="main-menu">
                    <nav role="navigation" class="widescreen clearfix">
                        <ul class="inline-listright font-nav ">


                            <li class="nav-item dropdown">
                                <a class="nav-item-link" href="/collections/all">
                                    Shop <!--<span aria-hidden="true" class="glyph arrow-down"></span>-->
                                </a>
                                <?php if ($categories) { ?>
                                <ul class="sub-nav catalog">
                                    <li class="sub-nav-item row">
                                        <?php foreach($categories as $category) { ?>
                                            <?php for($i=0; $i<count($category); $i++) { ?>
                                                <div class="small-2 columns">
                                                    <h3 class="title"><?php echo $category[$i]['parent_name']; ?></h3>
                                                    <?php if($category[$i]['children']){ ?>
                                                    <ul>
                                                        <?php foreach($category[$i]['children'] as $child) { ?>
                                                        <li><a href="<?php echo $child['href']; ?>" title=""><?php echo $child['name']; ?></a></li>
                                                        <?php } ?>
                                                        <li><a href="<?php echo $category[$i]['parent_link']; ?>" title="">View All</a></li>
                                                    </ul>
                                                    <?php } ?>
                                                </div>
                                            <?php } ?>
                                        <?php } ?>

                                    </li>
                                </ul>

                                <?php }//end-if-categories ?>
                            </li>


                        </ul>
                    </nav>

                </div>

            </div>

            <h1 class="title clearfix" role="banner">

                <p><a href="/" role="banner" title="Tanner Goods">
                        <?php if ($logo) { ?>
                                  <a href="<?php echo $home; ?>"><img src="<?php echo $logo; ?>" title="<?php echo $name; ?>" alt="<?php echo $name; ?>" class="img-responsive"/></a>
                                  <?php } else { ?>
                                  <h1><a href="<?php echo $home; ?>"><?php echo $name; ?></a></h1>
                                  <?php } ?>
                    </a></p>

            </h1>

            <div class="hidetabletright">
                <div class="main-menu">
                    <nav role="navigation" class="widescreen clearfix">
                        <ul class="inline-list font-nav">


                            <li class="nav-item">
                                <a class="nav-item-link" href="/pages/our-story">
                                    Story
                                </a>
                            </li>


                        </ul>
                    </nav>

                </div>

            </div>

        </div><!--centernav-->

        <div class="hidemob rgtmenu large-3 columns abel14" style="margin-top:33px; ">
            <ul style="padding-left: 0px;">

                <li class="search-account">
                    <div class="menu">

                        <a class="searchbar-open" href="#">SEARCH</a>

                        <span class="clear">|</span>

                        <a class="account-open" href="#">ACCOUNT</a>


                    </div>

                    <div class="searchbar-container">
                        <form action="/search" method="get" role="search">
                            <input type="hidden" name="type" value="product"/>
                            <input name="q" type="text" placeholder="Search..." class="search-box hint text"/>

                            <button class="glyph search"></button>
                        </form>
                        <a href="#" aria-hidden="true" class="searchbar-close glyph cross"></a>
                    </div>

                    <div class="account-container hidepad">

                        <a href="/account/login" id="customer_login_link">Log in</a> or <a href="/account/register"
                                                                                           id="customer_register_link">Sign
                            Up</a>

                        <a href="#" aria-hidden="true" class="account-close glyph cross"></a>
                    </div>


                </li>

                <span class="clear">|</span>
                <li class="cart-container">
                    <div class="cart">
                        <a class="cart-link abel15" href="/cart">Cart </a>

                        <div class="recently-added">
                            <table width="100%">
                                <thead>
                                <tr>
                                    <td colspan="3">Recently Added</td>
                                </tr>
                                </thead>

                                <tbody>


                                </tbody>

                                <tfoot>
                                <tr>
                                    <td class="items-count"><a href="/cart"><span class="number abel15">0</span>
                                            Items</a></td>
                                    <td colspan="2" class="text-right"><strong>TOTAL <span class="total-price"
                                                                                           style="margin-left:5px;"><span
                                                        class='money'>$0.00</span></span></strong></td>
                                </tr>
                                </tfoot>

                            </table>

                            <div class="row">
                                <div class="checkout columns">
                                    <a class="button tannerbutton" href="/cart">Go to Checkout</a>
                                </div><!-- #cart-meta -->
                            </div>

                            <div class="error">
                                <p>The item you just added is unavailable. Please select another product or variant.</p>
                            </div>

                        </div>
                    </div>

                </li>


            </ul>
        </div><!--large-->


    </div><!-- .row -->


    <!--
  <div class="bottom-row">
        <div class="row">
          <div class="columns menu-container">
            <div class="main-menu">
    <nav role="navigation" class="widescreen clearfix">
      <ul class="inline-list font-nav">








              <li class="nav-item dropdown">
                <a class="nav-item-link" href="/collections/all">
                  SHOP COLLECTION <span aria-hidden="true" class="glyph arrow-down"></span>
                </a>

                <ul class="sub-nav catalog">
                  <li class="sub-nav-item row">

                      <div class="columns large-3">

                          <h3 class="title">Tanner Goods</h3>

                        <ul>

                             <li><a href="/collections/customize-your-goods" title="">Customize Your Goods</a></li>

                             <li><a href="/collections/home-in-the-pacific" title="">Home in the Pacific</a></li>

                             <li><a href="/collections/limited-collections" title="">Exclusive Editions</a></li>

                             <li><a href="/collections/collaborations" title="">Collaborations</a></li>

                             <li><a href="/collections/everyday-carry" title="">Everyday Carry</a></li>

                             <li><a href="/collections/women-s-capsule" title="">Women's Capsule</a></li>

                             <li><a href="/collections/all" title="">View All</a></li>

                        </ul>
                      </div>



                      <div class="columns large-3">

                          <h3 class="title">Tanner Goods</h3>

                        <ul>

                             <li><a href="/collections/bags" title="">Bags</a></li>

                             <li><a href="/collections/belts" title="">Belts</a></li>

                             <li><a href="/collections/wallets" title="">Wallets</a></li>

                             <li><a href="/collections/lifestyle" title="">Lifestyle</a></li>

                             <li><a href="/collections/accessories" title="">Accessories</a></li>

                             <li><a href="/collections/canine" title="">Canine</a></li>

                             <li><a href="/collections/all" title="">View All</a></li>

                        </ul>
                      </div>



                      <div class="columns large-3">

                          <h3 class="title">Woodlands</h3>

                        <ul>

                             <li><a href="/collections/athletic" title="">Athletic</a></li>

                             <li><a href="/collections/casual" title="">Casual</a></li>

                             <li><a href="/collections/made-in-usa" title="">Made in USA</a></li>

                             <li><a href="/collections/house-label" title="">House Label</a></li>

                             <li><a href="/collections/outdoor-gear" title="">Outdoor Gear</a></li>

                             <li><a href="/collections/brands" title="">Brands</a></li>

                             <li><a href="/collections/view-all" title="">View All</a></li>

                        </ul>
                      </div>



                      <div class="columns large-3">

                          <h3 class="title">Woodlands</h3>

                        <ul>

                             <li><a href="/collections/sale" title="">Sale</a></li>

                             <li><a href="/collections/tops" title="">Tops</a></li>

                             <li><a href="/collections/denim" title="">Denim</a></li>

                             <li><a href="/collections/bottoms" title="">Bottoms</a></li>

                             <li><a href="/collections/footwear" title="">Footwear</a></li>

                             <li><a href="/collections/accessories-1" title="">Accessories</a></li>

                             <li><a href="/collections/view-all" title="">View All</a></li>

                        </ul>
                      </div>


                  </li>
                </ul>
              </li>










              <li class="nav-item">
                <a class="nav-item-link" href="/pages/our-story">
                  OUR STORY
                </a>
              </li>










              <li class="nav-item">
                <a class="nav-item-link" href="/pages/featured-journal">
                  JOURNAL
                </a>
              </li>



      </ul>
    </nav>

  </div>

          </div>
  -->

    <!-- <div class="columns cart-container">
       <div class="cart">
<a class="cart-link abel15" href="/cart">Cart </a>

 <div class="recently-added">
   <table width="100%">
       <thead>
         <tr>
           <td colspan="3">Recently Added</td>
         </tr>
       </thead>

       <tbody>


       </tbody>

       <tfoot>
         <tr>
                 <td class="items-count"><a href="/cart"><span class="number abel15">0</span> Items</a></td>
                 <td colspan="2" class="text-right"><strong>TOTAL <span class="total-price" style="margin-left:5px;"><span class='money'>$0.00</span></span></strong></td>
         </tr>
       </tfoot>

     </table>

     <div class="row">
       <div class="checkout columns">
         <a class="button tannerbutton" href="/cart">Go to Checkout</a>
       </div><!-- #cart-meta -->
    </div>

    <div class="error">
        <p>The item you just added is unavailable. Please select another product or variant.</p>
    </div>

    </div>
    </div>


    </div> -->

    <div class="mobile-tools">
        <a class="glyph menu" href=""></a>
        <a href="/search" class="glyph search"></a>

        <a href="/account" class="glyph account"></a>

        <a href="/cart" class="glyph cart"></a>
    </div>


    <div class="main-menu-dropdown-panel">

        <div class="menudrop">
            <div class="row">


            </div>
        </div>
    </div>


    <div class="mobile-menu">
        <nav role="navigation" style="display:block !important;">
            <ul class="font-nav">


                <li class="nav-item dropdown">
                    <a class="dropdown-link" href="/collections/all">
                        SHOP COLLECTION <span aria-hidden="true" class="glyph plus"></span><span aria-hidden="true"
                                                                                                 class="glyph minus"></span>
                    </a>

                    <ul class="sub-nav catalog">
                        <li class="bg"></li>

                        <li class="sub-nav-item">
                            <a class="dropdown-link" href="">Tanner Goods <span aria-hidden="true"
                                                                                class="glyph plus"></span><span
                                        aria-hidden="true" class="glyph minus"></span></a>
                            <ul class="sub-nav">
                                <li class="bg"></li>

                                <li><a href="/collections/customize-your-goods" title="">Customize Your Goods</a></li>

                                <li><a href="/collections/home-in-the-pacific" title="">Home in the Pacific</a></li>

                                <li><a href="/collections/limited-collections" title="">Exclusive Editions</a></li>

                                <li><a href="/collections/collaborations" title="">Collaborations</a></li>

                                <li><a href="/collections/everyday-carry" title="">Everyday Carry</a></li>

                                <li><a href="/collections/women-s-capsule" title="">Women's Capsule</a></li>

                                <li><a href="/collections/all" title="">View All</a></li>

                            </ul>
                        </li>


                        <li class="sub-nav-item">
                            <a class="dropdown-link" href="">Tanner Goods <span aria-hidden="true"
                                                                                class="glyph plus"></span><span
                                        aria-hidden="true" class="glyph minus"></span></a>
                            <ul class="sub-nav">
                                <li class="bg"></li>

                                <li><a href="/collections/bags" title="">Bags</a></li>

                                <li><a href="/collections/belts" title="">Belts</a></li>

                                <li><a href="/collections/wallets" title="">Wallets</a></li>

                                <li><a href="/collections/lifestyle" title="">Lifestyle</a></li>

                                <li><a href="/collections/accessories" title="">Accessories</a></li>

                                <li><a href="/collections/canine" title="">Canine</a></li>

                                <li><a href="/collections/all" title="">View All</a></li>

                            </ul>
                        </li>


                        <li class="sub-nav-item">
                            <a class="dropdown-link" href="">Woodlands <span aria-hidden="true"
                                                                             class="glyph plus"></span><span
                                        aria-hidden="true" class="glyph minus"></span></a>
                            <ul class="sub-nav">
                                <li class="bg"></li>

                                <li><a href="/collections/athletic" title="">Athletic</a></li>

                                <li><a href="/collections/casual" title="">Casual</a></li>

                                <li><a href="/collections/made-in-usa" title="">Made in USA</a></li>

                                <li><a href="/collections/house-label" title="">House Label</a></li>

                                <li><a href="/collections/outdoor-gear" title="">Outdoor Gear</a></li>

                                <li><a href="/collections/brands" title="">Brands</a></li>

                                <li><a href="/collections/view-all" title="">View All</a></li>

                            </ul>
                        </li>


                        <li class="sub-nav-item">
                            <a class="dropdown-link" href="">Woodlands <span aria-hidden="true"
                                                                             class="glyph plus"></span><span
                                        aria-hidden="true" class="glyph minus"></span></a>
                            <ul class="sub-nav">
                                <li class="bg"></li>

                                <li><a href="/collections/sale" title="">Sale</a></li>

                                <li><a href="/collections/tops" title="">Tops</a></li>

                                <li><a href="/collections/denim" title="">Denim</a></li>

                                <li><a href="/collections/bottoms" title="">Bottoms</a></li>

                                <li><a href="/collections/footwear" title="">Footwear</a></li>

                                <li><a href="/collections/accessories-1" title="">Accessories</a></li>

                                <li><a href="/collections/view-all" title="">View All</a></li>

                            </ul>
                        </li>


                    </ul>
                </li>


                <li class="nav-item">
                    <a class="nav-item-link" href="/pages/our-story">
                        OUR STORY
                    </a>
                </li>


                <li class="nav-item">
                    <a class="nav-item-link" href="/pages/featured-journal">
                        JOURNAL
                    </a>
                </li>


            </ul>
        </nav>

    </div>

    <div class="hidetablet columns secondmenu-container" style="margin-top: 0px;">
        <div class="main-menu">
            <nav role="navigation" class="widescreen clearfix">

                <div class="tablenav">
                    <ul class="inline-list font-nav horizontal-list">


                        <li class="nav-item">
                            <p style="margin-bottom: 0px;"><a class="nav-item-link" href="/pages/find-a-dealer">
                                    Find a Store
                                </a></p>
                        </li>


                        <li class="nav-item">
                            <p style="margin-bottom: 0px;"><a class="nav-item-link" href="/pages/sizing">
                                    Sizing
                                </a></p>
                        </li>


                        <li class="nav-item">
                            <p style="margin-bottom: 0px;"><a class="nav-item-link" href="/pages/care">
                                    Care
                                </a></p>
                        </li>


                        <li class="nav-item">
                            <p style="margin-bottom: 0px;"><a class="nav-item-link" href="#">
                                    Watch
                                </a></p>
                        </li>


                        <li class="nav-item">
                            <p style="margin-bottom: 0px;"><a class="nav-item-link" href="#">
                                    Listen
                                </a></p>
                        </li>


                    </ul>
                </div>
            </nav>

        </div>

    </div>

    <div class="row">
        <div class="header-divider"></div>
    </div>

</header>
<script type="text/javascript">
    $(document).ready(function(){
        $( ".main-menu .small-2:first-child" )
          .addClass("large-offset-2");
    })
</script>