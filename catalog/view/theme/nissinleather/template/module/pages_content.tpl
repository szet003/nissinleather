<?php if($full_size == 1 ) { ?>
<div class="row full-width">
    <div class="columns">

        <div class="page-content">
            <?php echo $description; ?>
        </div>
    </div>

</div>
</div>
<?php } else { ?>
<div class="columns large-9 ">
    <div class="page-content rte-content colored-links">
        <?php echo $description; ?>
    </div>
</div>
<?php } ?>

