
<div class="full-width row featuredcollection aboutPageWebowsk" >
  <div class="aboutSection" id="aboutLeft">
    <?php foreach($firsthalf as $left) { ?>
    <a href="<?php echo $left['href']; ?>">
      <div class="aboutPanelWrap">
        <div class="bs sw" data-aspect="1.8" data-img="<?php echo $left['image']; ?>" ></div>
        <div class="abOver trn2">
          <div class="table">
            <div class="cell">
              <p class="subTitle"><?php echo $left['sub_title']; ?></p>
              <div class="title"><?php echo $left['name']; ?></div>
              <div class="trn2 ctabutton">READ MORE</div>
            </div>
          </div>
        </div>
      </div>
    </a>
    <?php } ?>

  </div><!--aboutLeft-->

  <div class="aboutSection" id="aboutRight">
    <?php foreach($secondhalf as $right) { ?>
    <a href="<?php echo $right['href']; ?>">
      <div class="aboutPanelWrap">
        <div class="bs sw" data-aspect="1.8" data-img="<?php echo $right['image']; ?>"></div>
        <div class="abOver trn2">
          <div class="table">
            <div class="cell">
              <p class="subTitle"><?php echo $right['sub_title']; ?></p>
              <div class="title"><?php echo $right['name']; ?></div>
              <div class="trn2 ctabutton hidemob">read more</div>
            </div>
          </div>
        </div>
      </div>
    </a>
    <?php } ?>

  </div><!--aboutRight-->



</div><!--featuredcollection-->
