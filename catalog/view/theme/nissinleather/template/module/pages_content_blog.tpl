
  <div class="full-width row featuredcollection">
    <?php if($count_images > 1) { ?>
    <div class="flexslider">
      <ul class="slides">
        <?php foreach($images as $image) { ?>
        <li><img src="<?php echo $image['thumb']; ?>"/>
          <p class="flex-caption"><?php echo $image['caption']; ?></p></li>
        <?php } ?>
      </ul>
    </div>


    <script type="text/javascript">
      $(window).load(function () {
        $('.flexslider').flexslider({
          animation: "slide",
          controlNav: true,
          directionNav: true,
          prevText: " ",
          nextText: " ",
          slideshowSpeed: 5000,
        });
      });
    </script>
    <?php } elseif($count_images == 1) { ?>
    <div class="lngcollectionstatic">
      <?php foreach($images as $image) { ?>
      <img src="<?php echo $image['thumb']; ?>"/>
      <div class="title"><?php echo $image['caption']; ?></div>
      <?php } ?>
    </div><!--lng_img-->
    <?php } ?>

    <?php if($count_images > 1) { ?>
    <div class="large-10 push-1 rte-content abel18 colored-links " style="text-align:center; padding: 1.5% 7% 0;">
      <?php echo html_entity_decode($pages['description']); ?>
    </div>
    <?php } else { ?>
    <div class="large-8 push-2 rte-content colored-links abel18" style="text-align:center;">
      <?php echo strip_tags(html_entity_decode(strip_tags($pages['description']))); ?>
    </div>
    <?php }  ?>

  </div>
