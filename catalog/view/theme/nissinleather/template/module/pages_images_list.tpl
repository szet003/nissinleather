<?php if(isset($banners) && !empty($banners)) { ?>

    <div class="full-width row featuredcollection">


        <h1 class="page-title" style="margin-bottom: 0px; display:none;">FEATURED COLLECTIONS</h1>

        <?php foreach($banners as $banner){ ?>
        <a href="<?php echo $banner['link']; ?>">
            <div class="lngcollection">
                <img src="<?php echo $banner['image']; ?>"/>
                <div class="title"><?php echo $banner['title']; ?></div>
                <div class="ctabutton hidemob"><?php echo $banner['link_title']; ?></div>
            </div><!--lng_img-->
        </a>
        <?php } ?>


    </div>


<?php } ?>