<?php if(isset($banners) && !empty($banners)) { ?>
<section class="slider light">
    <div class="owl-buttons"><div class="owl-prev"></div><div class="owl-next"></div></div>
    <div class="row">
        <div class="slides">
            <?php foreach($banners as $banner) { ?>
            <div class="slide">
                <a href="<?php echo $banner['link']; ?>">
                    <img src="<?php echo $banner['image']; ?>" alt="" />
                </a>
            </div>
            <?php } ?>
        </div>
    </div>
</section>
<?php } ?>