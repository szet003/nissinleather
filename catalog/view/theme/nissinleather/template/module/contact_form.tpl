<div class="columns large-3">
    <form method="post" action="/contact#contact_form" id="contact_form" class="contact-form" accept-charset="UTF-8">
        <input type="hidden" value="contact" name="form_type"/><input type="hidden" name="utf8" value="✓"/>


        <h3>Additional Questions</h3>
        <!-- <p>Give us a call <a href="tel:1-503-862-3224" style="color:#c8b18b">1-503-862-3224</a></p>-->
        <p>
            <label>Your Name:</label>
            <input type="text" id="contactFormName" name="contact[name]" placeholder="Your name" class="styled-input"
                   value=""/>
        </p>
        <p>
            <label>Email:</label>
            <input required="required" type="email" id="contactFormEmail" name="contact[email]"
                   placeholder="your@email.com" class="styled-input" value=""/>
        </p>
        <p>
            <label>Phone Number:</label>
            <input type="tel" id="contactFormTelephone" name="contact[phone]" placeholder="555-555-1234"
                   class="styled-input" value=""/>
        </p>
        <p>
            <label>Message:</label>
            <textarea required="required" rows="10" cols="60" id="contactFormMessage" name="contact[body]"
                      placeholder="Your Message" class="styled-input"></textarea>
        </p>
        <p>
            <input class="button styled-submit tannerbutton" type="submit" id="contactFormSubmit" value="Send"/>
        </p>


    </form>
</div>