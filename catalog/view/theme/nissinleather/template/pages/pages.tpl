<?php echo $header; ?>
<section class="main-content">
<?php echo $content_top; ?>
    <?php if($column_left != '' && $column_right != '') { ?>
    <div class="fixed-row">
        <div class="columns large-12">
            <?php if($using_breadcrumbs == 1) { ?>
            <ul class="breadcrumbs colored-links abel14" style="padding-bottom: 0px;">
                <li><a href="<?php echo $breadcrumbs[0]['href']; ?>"
                       style="color:#c8b18b;"><?php echo $breadcrumbs[0]['text']; ?></a></li>
                <li><?php echo $breadcrumbs[1]['text']; ?></li>
            </ul>
            <?php } ?>

            <!--<h1 class="page-title">PORTLAND STORE</h1>-->
        </div>
    </div>

    <div class="row large-12 fixed-width">

        <?php echo $column_left; ?>
        <?php echo $column_right; ?>

    </div>
    <?php } ?>
</section>
<div class="footerpad"></div>
<?php echo $footer; ?>