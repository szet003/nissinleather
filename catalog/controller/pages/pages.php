<?php
class ControllerPagesPages extends Controller {
	public function index() {
		
		$this->load->language('pages/pages');

		$this->load->model('catalog/pages');

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home')
		);

		if (isset($this->request->get['pages_id'])) {
			$pages_id = (int)$this->request->get['pages_id'];
		} else {
			$pages_id = 0;
		}

		$pages_info = $this->model_catalog_pages->getpages($pages_id);

		if ($pages_info) {
			$this->document->setTitle($pages_info['meta_title']);
			$this->document->setDescription($pages_info['meta_description']);
			$this->document->setKeywords($pages_info['meta_keyword']);

			$data['using_breadcrumbs'] = $pages_info['using_breadcrumbs'];

			$data['breadcrumbs'][] = array(
				'text' => $pages_info['name'],
				'href' => $this->url->link('pages/pages', 'pages_id=' .  $pages_id)
			);

			$data['heading_title'] = $pages_info['name'];

			$data['button_continue'] = $this->language->get('button_continue');

			$data['description'] = html_entity_decode($pages_info['description'], ENT_QUOTES, 'UTF-8');

			$data['continue'] = $this->url->link('common/home');

			$data['column_left'] = $this->load->controller('common/column_left');
			$data['column_right'] = $this->load->controller('common/column_right');
			$data['content_top'] = $this->load->controller('common/content_top');
			$data['content_bottom'] = $this->load->controller('common/content_bottom');
			$data['footer'] = $this->load->controller('common/footer');
			$data['header'] = $this->load->controller('common/header');



			$this->response->setOutput($this->load->view('pages/pages', $data));
		} else {
			$data['breadcrumbs'][] = array(
				'text' => $this->language->get('text_error'),
				'href' => $this->url->link('pages/pages', 'pages_id=' . $pages_id)
			);

			$this->document->setTitle($this->language->get('text_error'));

			$data['heading_title'] = $this->language->get('text_error');

			$data['text_error'] = $this->language->get('text_error');

			$data['button_continue'] = $this->language->get('button_continue');

			$data['continue'] = $this->url->link('common/home');

			$this->response->addHeader($this->request->server['SERVER_PROTOCOL'] . ' 404 Not Found');

			$data['column_left'] = $this->load->controller('common/column_left');
			$data['column_right'] = $this->load->controller('common/column_right');
			$data['content_top'] = $this->load->controller('common/content_top');
			$data['content_bottom'] = $this->load->controller('common/content_bottom');
			$data['footer'] = $this->load->controller('common/footer');
			$data['header'] = $this->load->controller('common/header');

			$this->response->setOutput($this->load->view('error/not_found', $data));
		}
	}

	public function agree() {
		$this->load->model('catalog/pages');

		if (isset($this->request->get['pages_id'])) {
			$pages_id = (int)$this->request->get['pages_id'];
		} else {
			$pages_id = 0;
		}

		$output = '';

		$pages_info = $this->model_catalog_pages->getpages($pages_id);

		if ($pages_info) {
			$output .= html_entity_decode($pages_info['description'], ENT_QUOTES, 'UTF-8') . "\n";
		}

		$this->response->setOutput($output);
	}
}