<?php

class ControllerModulePagesContent extends Controller
{
    public function index($setting)
    {
        //$this->load->language('module/pages_content_blog');

        //$data['heading_title'] = $this->language->get('heading_title');

        if (isset($this->request->get['pages_id'])) {
            $parts = explode('_', (string)$this->request->get['pages_id']);
        } else {
            $parts = array();
        }

        if (isset($parts[0])) {
            $data['pages_id'] = $parts[0];
        } else {
            $data['pages_id'] = 0;
        }
        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => 'home',
            'href' => $this->url->link('common/home'),
            'type' => 'link'
        );
        $this->load->model('catalog/pages');
        $this->load->model('tool/image');

        $data['pages'] = array();

        $data['images'] = array();

        $results = $this->model_catalog_pages->getPagesImages($data['pages_id']);

        foreach ($results as $result) {
            $data['images'][] = array(
                'thumb' => $this->model_tool_image->resize($result['image'], 1200, 417),
                'caption' => $result['caption']
            );
        }

        $data['count_images'] = count($data['images']);
        $pages_id = $data['pages_id'];
        $pages = $this->model_catalog_pages->getPages($data['pages_id']);
        if ($pages) {

            $data['breadcrumbs'][] = array(
                'text' => $pages['name'],
                'href' => $this->url->link('pages/pages', 'pages_id=' . $pages_id),
                'type' => 'text'
            );

            $data['heading_name'] = $pages['name'];

            $data['button_continue'] = $this->language->get('button_continue');

            $data['description'] = html_entity_decode($pages['description'], ENT_QUOTES, 'UTF-8');

            $data['continue'] = $this->url->link('common/home');

        }

        $data['using_breadcrumbs'] = $setting['using_breadcrumbs'];
        $data['full_size'] = $setting['full_size'];

        return $this->load->view('module/pages_content', $data);
    }
}