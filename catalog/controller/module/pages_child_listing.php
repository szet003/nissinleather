<?php
class ControllerModulePagesChildListing extends Controller {
	public function index($setting) {
		$this->load->language('module/pages_child_listing');

		$data['heading_title'] = $this->language->get('heading_title');

		if (isset($this->request->get['pages_id'])) {
			$parts = explode('_', (string)$this->request->get['pages_id']);
		} else {
			$parts = array();
		}

		if (isset($parts[0])) {
			$data['pages_id'] = $parts[0];
		} else {
			$data['pages_id'] = 0;
		}

		$this->load->model('catalog/pages');
		$this->load->model('tool/image');
		$data['pagess'] = array();
        $pages_info = $this->model_catalog_pages->getPages($data['pages_id']);
		$pagess = $this->model_catalog_pages->getPagess($data['pages_id']);
        $data['pages_name'] = $pages_info['name'];
        $type = $setting['type'];
        if($type == 0) {
            $image_width = 800;
            $image_height = 444;
            $file_view = 'module/pages_child_listing_block';
        } else {
            $image_width = 2500;
            $image_height = 868;
            $file_view = 'module/pages_child_listing_row';
        }


		foreach ($pagess as $pages) {

			$data['pagess'][] = array(
				'pages_id' => $pages['pages_id'],
				'name'        => $pages['name'],
				'href'        => $this->url->link('pages/pages', 'pages_id=' . $pages['pages_id']),
				'sub_title' => $pages['sub_title'],
				'image'    => $this->model_tool_image->resize($pages['image'], $image_width, $image_height )
			);
		}
        $len = count($data['pagess']);
        $data['firsthalf'] = array_slice($data['pagess'], 0, $len / 2);
        $data['secondhalf'] = array_slice($data['pagess'], $len / 2);

        return $this->load->view($file_view, $data);


	}
}