<?php
class ControllerModulePagesContentBlog extends Controller {
	public function index() {
		//$this->load->language('module/pages_content_blog');

		//$data['heading_title'] = $this->language->get('heading_title');

		if (isset($this->request->get['pages_id'])) {
			$parts = explode('_', (string)$this->request->get['pages_id']);
		} else {
			$parts = array();
		}

		if (isset($parts[0])) {
			$data['pages_id'] = $parts[0];
		} else {
			$data['pages_id'] = 0;
		}

		$this->load->model('catalog/pages');
		$this->load->model('tool/image');

		$data['pages'] = array();

        $data['images'] = array();

        $results = $this->model_catalog_pages->getPagesImages($data['pages_id']);

        foreach ($results as $result) {
            $data['images'][] = array(
                'thumb' => $this->model_tool_image->resize($result['image'], 1200, 417),
                'caption' => $result['caption']
            );
        }

        $data['count_images'] = count($data['images']);

		$pages = $this->model_catalog_pages->getPages($data['pages_id']);
			$data['pages'] = array(
				'pages_id' => $pages['pages_id'],
				'name'        => $pages['name'],
				'href'        => $this->url->link('pages/pages', 'pages_id=' . $pages['pages_id']),
				'sub_title' => $pages['sub_title'],
				'image'    => $this->model_tool_image->resize($pages['image'], 800, 444 ),
                'description' => $pages['description']
			);



		return $this->load->view('module/pages_content_blog', $data);
	}
}