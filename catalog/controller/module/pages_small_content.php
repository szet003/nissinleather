<?php

class ControllerModulePagesSmallContent extends Controller
{
    public function index($setting)
    {


        if (isset($this->request->get['pages_id'])) {
            $parts = explode('_', (string)$this->request->get['pages_id']);
        } else {
            $parts = array();
        }

        if (isset($parts[0])) {
            $data['pages_id'] = $parts[0];
        } else {
            $data['pages_id'] = 0;
        }

        $this->load->model('catalog/pages');

        $data['pages'] = array();

        $pages = $this->model_catalog_pages->getPages($data['pages_id']);
        if ($pages) {

            $data['description'] = html_entity_decode($pages['description_small'], ENT_QUOTES, 'UTF-8');


        }


        return $this->load->view('module/pages_small_content', $data);
    }
}