<?php

class txt_ModelCatalogProduct extends ModelCatalogProduct
{
    public function addProduct($data)
    {
        $product_id = parent::addProduct($data);

        if (isset($data['big_image'])) {
            $this->db->query("UPDATE " . DB_PREFIX . "product SET big_image = '" . $this->db->escape($data['big_image']) . "' WHERE product_id = '" . (int)$product_id . "'");
        }

        $this->db->query("DELETE FROM " . DB_PREFIX . "product_description WHERE product_id = '" . (int)$product_id . "'");
        foreach ($data['product_description'] as $language_id => $value) {
            $this->db->query("INSERT INTO " . DB_PREFIX . "product_description SET product_id = '" . (int)$product_id . "', language_id = '" . (int)$language_id . "', name = '" . $this->db->escape($value['name']) . "', description = '" . $this->db->escape($value['description']) . "', tag = '" . $this->db->escape($value['tag']) . "', meta_title = '" . $this->db->escape($value['meta_title']) . "', meta_description = '" . $this->db->escape($value['meta_description']) . "', meta_keyword = '" . $this->db->escape($value['meta_keyword']) . "', details = '" . $this->db->escape($value['details']) . "', shipping_details = '" . $this->db->escape($value['shipping_details']) . "'");
        }
        return $product_id;
    }

    public function editProduct($product_id, $data)
    {
        parent::editProduct($product_id, $data);

        if (isset($data['big_image'])) {
            $this->db->query("UPDATE " . DB_PREFIX . "product SET big_image = '" . $this->db->escape($data['big_image']) . "' WHERE product_id = '" . (int)$product_id . "'");
        }

        $this->db->query("DELETE FROM " . DB_PREFIX . "product_description WHERE product_id = '" . (int)$product_id . "'");
        foreach ($data['product_description'] as $language_id => $value) {
            $this->db->query("INSERT INTO " . DB_PREFIX . "product_description SET product_id = '" . (int)$product_id . "', language_id = '" . (int)$language_id . "', name = '" . $this->db->escape($value['name']) . "', description = '" . $this->db->escape($value['description']) . "', tag = '" . $this->db->escape($value['tag']) . "', meta_title = '" . $this->db->escape($value['meta_title']) . "', meta_description = '" . $this->db->escape($value['meta_description']) . "', meta_keyword = '" . $this->db->escape($value['meta_keyword']) . "', details = '" . $this->db->escape($value['details']) . "', shipping_details = '" . $this->db->escape($value['shipping_details']) . "'");
        }
        return $product_id;
    }

    public function getProductDescriptions($product_id)
    {
        $product_description_data = array();

        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_description WHERE product_id = '" . (int)$product_id . "'");

        foreach ($query->rows as $result) {
            $product_description_data[$result['language_id']] = array(
                'name' => $result['name'],
                'description' => $result['description'],
                'meta_title' => $result['meta_title'],
                'meta_description' => $result['meta_description'],
                'meta_keyword' => $result['meta_keyword'],
                'tag' => $result['tag'],
                'details' => $result['details'],
                'shipping_details' => $result['shipping_details']
            );
        }

        return $product_description_data;
    }

}
