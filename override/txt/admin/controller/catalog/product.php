<?php

class txt_ControllerCatalogProduct extends ControllerCatalogProduct
{
    function add()
    {
        $language_id = 1;// default SEO link in English
        if (isset($this->request->post['keyword']) && utf8_strlen($this->request->post['keyword']) <= 0) {
            if (parent::validateForm()) {
                $this->request->post['keyword'] = $this->Slug($this->request->post['product_description'][$language_id]['name'] . ' ' . $this->request->post['model']);
            }
        }
        parent::add();
    }

    public function preRender($template_buffer, $template_name, &$data)
    {
        if ($template_name != 'catalog/product_form.tpl') {
            return parent::preRender($template_buffer, $template_name, $data);
        }

        $data['entry_details'] = $this->language->get('entry_details');
        $data['entry_shipping_details'] = $this->language->get('entry_shipping_details');
        $data['entry_big_image'] = $this->language->get('entry_big_image');

        $this->load->model('catalog/product');

        if (isset($this->request->get['product_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
            $product_info = $this->model_catalog_product->getProduct($this->request->get['product_id']);
        }
        
        // Image
        if (isset($this->request->post['big_image'])) {
            $data['big_image'] = $this->request->post['big_image'];
        } elseif (!empty($product_info)) {
            $data['big_image'] = $product_info['big_image'];
        } else {
            $data['big_image'] = '';
        }

        $this->load->model('tool/image');

        if (isset($this->request->post['big_image']) && is_file(DIR_IMAGE . $this->request->post['big_image'])) {
            $data['thumb_big_image'] = $this->model_tool_image->resize($this->request->post['big_image'], 100, 100);
        } elseif (!empty($product_info) && is_file(DIR_IMAGE . $product_info['big_image'])) {
            $data['thumb_big_image'] = $this->model_tool_image->resize($product_info['big_image'], 100, 100);
        } else {
            $data['thumb_big_image'] = $this->model_tool_image->resize('no_image.png', 100, 100);
        }


        //EDIT PRODUCT FORM VIEW
        $this->load->helper('modifier');

        $search = '<li><a href="#language<?php echo $language[\'language_id\']; ?>" data-toggle="tab"><img src="language/<?php echo $language[\'code\']; ?>/<?php echo $language[\'code\']; ?>.png" title="<?php echo $language[\'name\']; ?>" /> <?php echo $language[\'name\']; ?></a></li>';
        $add = '<?php 
						switch($language[\'code\']) {
							//Add Here your opencart language code if is different of the list in module description in opencart.com web site
							case \'vi-vn\':
								$langC[$language[\'language_id\']] = \'vi\';
								break;
							default:
								$langC[$language[\'language_id\']] = $language[\'code\'];
						}
					?>';
        $offset = 1;
        $index = 0;

        $template_buffer = Modifier::modifyStringBuffer($template_buffer, $search, $add, 'before');

        $search = 'var attribute_row = <?php echo $attribute_row; ?>;';
        $add = '$(document).ready( function() {
						<?php foreach ($languages as $language) { ?>
						$("#title-slug<?php echo $langC[$language[\'language_id\']]; ?>").stringToSlug({
							setEvents: \'keyup keydown blur\',
							getPut: \'#slug-result<?php echo $langC[$language[\'language_id\']]; ?>\',
							options: {
								lang: \'<?php echo $langC[$language[\'language_id\']]; ?>\',
								titleCase: false
							}
						});
						$("#title-slug<?php echo $langC[$language[\'language_id\']]; ?>").stringToSlug({
							setEvents: \'keyup keydown blur\',
							space: \' \',
							options: {
								lang: \'<?php echo $langC[$language[\'language_id\']]; ?>\',
								titleCase: true
							},
							callback: function(str) {
								$(\'.cMeta<?php echo $langC[$language[\'language_id\']]; ?>\').val($("#title-slug<?php echo $langC[$language[\'language_id\']]; ?>").val());
							}
						});
						<?php } ?>
					});';

        $template_buffer = Modifier::modifyStringBuffer($template_buffer, $search, $add, 'after');

        $search = 'name="product_description[<?php echo $language[\'language_id\']; ?>][name]"';
        $add = 'id="title-slug<?php echo $langC[$language[\'language_id\']]; ?>" name="product_description[<?php echo $language[\'language_id\']; ?>][name]"';

        $template_buffer = Modifier::modifyStringBuffer($template_buffer, $search, $add, 'replace');

        $search = 'id="input-meta-title<?php echo $language[\'language_id\']; ?>" class="form-control"';
        $add = 'id="input-meta-title<?php echo $language[\'language_id\']; ?>" class="form-control cMeta<?php echo $langC[$language[\'language_id\']]; ?>"';

        $template_buffer = Modifier::modifyStringBuffer($template_buffer, $search, $add, 'replace');

        $search = '<textarea name="product_description[<?php echo $language[\'language_id\']; ?>][description]" placeholder="<?php echo $entry_description; ?>" id="input-description<?php echo $language[\'language_id\']; ?>" class="form-control summernote"><?php echo isset($product_description[$language[\'language_id\']]) ? $product_description[$language[\'language_id\']][\'description\'] : \'\'; ?></textarea>';
        $add = '
<div class="form-group">
                    <label class="col-sm-2 control-label" for="input-details<?php echo $language[\'language_id\']; ?>"><?php echo $entry_details; ?></label>
                    <div class="col-sm-10">
<textarea name="product_description[<?php echo $language[\'language_id\']; ?>][details]"
                                      placeholder="<?php echo $entry_details; ?>"
                                      id="input-description<?php echo $language[\'language_id\']; ?>"
                                      class="form-control summernote"><?php echo isset($product_description[$language[\'language_id\']]) ? $product_description[$language[\'language_id\']][\'details\'] : \'\'; ?></textarea></div></div>
                                    
                                <div class="form-group">
                    <label class="col-sm-2 control-label" for="input-shipping_shipping_details<?php echo $language[\'language_id\']; ?>"><?php echo $entry_shipping_details; ?></label>
                    <div class="col-sm-10">
<textarea name="product_description[<?php echo $language[\'language_id\']; ?>][shipping_details]"
                                      placeholder="<?php echo $entry_shipping_details; ?>"
                                      id="input-description<?php echo $language[\'language_id\']; ?>"
                                      class="form-control summernote"><?php echo isset($product_description[$language[\'language_id\']]) ? $product_description[$language[\'language_id\']][\'shipping_details\'] : \'\'; ?></textarea></div></div>';

        $template_buffer = Modifier::modifyStringBuffer($template_buffer, $search, $add, 'after', 2);

        $search = '<td class="text-left"><a href="" id="thumb-image" data-toggle="image" class="img-thumbnail"><img src="<?php echo $thumb; ?>" alt="" title="" data-placeholder="<?php echo $placeholder; ?>" /></a><input type="hidden" name="image" value="<?php echo $image; ?>" id="input-image" /></td>';
        $add = '<div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover">
                          <thead>
                            <tr>
                              <td class="text-left"><?php echo $entry_big_image; ?></td>
                            </tr>
                          </thead>
        
                          <tbody>
                            <tr>
                              <td class="text-left"><a href="" id="thumb-big_image" data-toggle="image" class="img-thumbnail"><img src="<?php echo $thumb_big_image; ?>" alt="" title="" data-placeholder="<?php echo $placeholder; ?>" /></a><input type="hidden" name="big_image" value="<?php echo $big_image; ?>" id="input-big_image" /></td>
                          </tr>
                          </tbody>
                        </table>
                      </div> ';

        $template_buffer = Modifier::modifyStringBuffer($template_buffer, $search, $add, 'after', 4);

        return parent::preRender($template_buffer, $template_name, $data);
    }

    function Slug($string)
    {
        return strtolower(trim(preg_replace('~[^0-9a-z]+~i', '-', html_entity_decode(preg_replace('~&([a-z]{1,2})(?:acute|cedil|circ|grave|lig|orn|ring|slash|th|tilde|uml);~i', '$1', htmlentities($string, ENT_QUOTES, 'UTF-8')), ENT_QUOTES, 'UTF-8')), '-'));
    }

}
