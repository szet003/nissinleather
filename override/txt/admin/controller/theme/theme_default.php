<?php

class txt_ControllerThemeThemeDefault extends ControllerThemeThemeDefault
{
    private $error = array();
    public function preRender($template_buffer, $template_name, &$data)
    {
        if ($template_name != 'theme/theme_default.tpl') {
            return parent::preRender($template_buffer, $template_name, $data);
        }

        $data['entry_big_image_product'] = $this->language->get('entry_big_image_product');

        if (isset($this->error['big_image_product'])) {
            $data['error_big_image_product'] = $this->error['big_image_product'];
        } else {
            $data['error_big_image_product'] = '';
        }

        if (isset($this->request->get['store_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
            $setting_info = $this->model_setting_setting->getSetting('theme_default', $this->request->get['store_id']);
        }

        if (isset($this->request->post['theme_default_big_image_product_width'])) {
            $data['theme_default_big_image_product_width'] = $this->request->post['theme_default_big_image_product_width'];
        } elseif (isset($setting_info['theme_default_big_image_product_width'])) {
            $data['theme_default_big_image_product_width'] = $this->config->get('theme_default_big_image_product_width');
        } else {
            $data['theme_default_big_image_product_width'] = 1891;
        }

        if (isset($this->request->post['theme_default_big_image_product_height'])) {
            $data['theme_default_big_image_product_height'] = $this->request->post['theme_default_big_image_product_height'];
        } elseif (isset($setting_info['theme_default_big_image_product_height'])) {
            $data['theme_default_big_image_product_height'] = $this->config->get('theme_default_big_image_product_height');
        } else {
            $data['theme_default_big_image_product_height'] = 827;
        }

        // Sửa view menu
        $this->load->helper('modifier');

        $search = '<div class="text-danger"><?php echo $error_image_product; ?></div>';
        $add = '<div class="form-group required">
                      <label class="col-sm-2 control-label" for="input-image-product-width"><?php echo $entry_big_image_product; ?></label>
                      <div class="col-sm-10">
                        <div class="row">
                          <div class="col-sm-6">
                            <input type="text" name="theme_default_big_image_product_width" value="<?php echo $theme_default_big_image_product_width; ?>" placeholder="<?php echo $entry_width; ?>" id="input-image-product-width" class="form-control" />
                          </div>
                          <div class="col-sm-6">
                            <input type="text" name="theme_default_big_image_product_height" value="<?php echo $theme_default_big_image_product_height; ?>" placeholder="<?php echo $entry_height; ?>" class="form-control" />
                          </div>
                        </div>
                        <?php if ($error_big_image_product) { ?>
                        <div class="text-danger"><?php echo $error_big_image_product; ?></div>
                        <?php } ?>
                      </div>
                    </div>';

        $template_buffer = Modifier::modifyStringBuffer($template_buffer, $search, $add, 'after', 3);

        return parent::preRender($template_buffer, $template_name, $data);
    }

    protected function validate()
    {
        parent::validate();

        if (!$this->request->post['theme_default_big_image_product_width'] || !$this->request->post['theme_default_big_image_product_height']) {
            $this->error['big_image_product'] = $this->language->get('error_big_image_product');
        }

        return !$this->error;
    }
}
