<?php
class txt_ControllerDesignBanner extends ControllerDesignBanner {
	public function preRender( $template_buffer, $template_name, &$data ) {
		if ($template_name != 'design/banner_form.tpl') {
			return parent::preRender( $template_buffer, $template_name, $data );
		}

        $data['entry_sub_title'] = $this->language->get('entry_sub_title');
        $data['entry_link_title'] = $this->language->get('entry_link_title');

		// Sửa view menu
		$search = '<td class="text-left"><?php echo $entry_title; ?></td>';
		$add  = '<td class="text-left"><?php echo $entry_sub_title; ?></td>';
		$this->load->helper( 'modifier' );
		$template_buffer = Modifier::modifyStringBuffer( $template_buffer,$search,$add,'after' );

        $search = '<td class="text-left"><?php echo $entry_link; ?></td>';
        $add  = '<td class="text-left"><?php echo $entry_link_title; ?></td>';
        $this->load->helper( 'modifier' );
        $template_buffer = Modifier::modifyStringBuffer( $template_buffer,$search,$add,'after' );

		$search = '<input type="text" name="banner_image[<?php echo $image_row; ?>][banner_image_description][<?php echo $language[\'language_id\']; ?>][title]" value="<?php echo isset($banner_image[\'banner_image_description\'][$language[\'language_id\']]) ? $banner_image[\'banner_image_description\'][$language[\'language_id\']][\'title\'] : \'\'; ?>" placeholder="<?php echo $entry_title; ?>" class="form-control" />';
		$add  = ' <td class="text-left">
                       <?php foreach ($languages as $language) { ?>
                       <div class="input-group pull-left"><span class="input-group-addon"><img src="view/image/flags/<?php echo $language[\'image\']; ?>" sub_title="<?php echo $language[\'name\']; ?>" /> </span>
                           <input type="text" name="banner_image[<?php echo $image_row; ?>][banner_image_description][<?php echo $language[\'language_id\']; ?>][sub_title]" value="<?php echo isset($banner_image[\'banner_image_description\'][$language[\'language_id\']]) ? $banner_image[\'banner_image_description\'][$language[\'language_id\']][\'sub_title\'] : \'\'; ?>" placeholder="<?php echo $entry_sub_title; ?>" class="form-control" />
                       </div>
                       <?php if (isset($error_banner_image[$image_row][$language[\'language_id\']])) { ?>
                       <div class="text-danger">
                           <?php echo $error_banner_image[$image_row][$language[\'language_id\']]; ?>
                       </div>
                       <?php } ?>
                       <?php } ?>
                   </td>';
        $offset = 5;
		$this->load->helper( 'modifier' );
		$template_buffer = Modifier::modifyStringBuffer( $template_buffer,$search,$add,'after',$offset );

        $search = '<td class="text-left" style="width: 30%;"><input type="text" name="banner_image[<?php echo $image_row; ?>][link]" value="<?php echo $banner_image[\'link\']; ?>" placeholder="<?php echo $entry_link; ?>" class="form-control" /></td>';
        $add  = ' <td class="text-left">
                       <?php foreach ($languages as $language) { ?>
                       <div class="input-group pull-left"><span class="input-group-addon"><img src="view/image/flags/<?php echo $language[\'image\']; ?>" link_title="<?php echo $language[\'name\']; ?>" /> </span>
                           <input type="text" name="banner_image[<?php echo $image_row; ?>][banner_image_description][<?php echo $language[\'language_id\']; ?>][link_title]" value="<?php echo isset($banner_image[\'banner_image_description\'][$language[\'language_id\']]) ? $banner_image[\'banner_image_description\'][$language[\'language_id\']][\'link_title\'] : \'\'; ?>" placeholder="<?php echo $entry_link_title; ?>" class="form-control" />
                       </div>
                       <?php if (isset($error_banner_image[$image_row][$language[\'language_id\']])) { ?>
                       <div class="text-danger">
                           <?php echo $error_banner_image[$image_row][$language[\'language_id\']]; ?>
                       </div>
                       <?php } ?>
                       <?php } ?>
                   </td>';
        $this->load->helper( 'modifier' );
        $template_buffer = Modifier::modifyStringBuffer( $template_buffer,$search,$add,'after');



        $search = 'html += \'  <td class="text-left" style="width: 30%;"><input type="text" name="banner_image[\' + image_row + \'][link]" value="" placeholder="<?php echo $entry_link; ?>" class="form-control" /></td>\';';
        $add  = '  html += \'  <td class="text-left">\';
                        	<?php foreach ($languages as $language) { ?>
                        	html += \'    <div class="input-group">\';
                        	html += \'      <span class="input-group-addon"><img src="view/image/flags/<?php echo $language[\'image\']; ?>" sub_title="<?php echo $language[\'name\']; ?>" /></span><input type="text" name="banner_image[\' + image_row + \'][banner_image_description][<?php echo $language[\'language_id\']; ?>][sub_title]" value="" placeholder="<?php echo $entry_sub_title; ?>" class="form-control" />\';
                            html += \'    </div>\';
                        	<?php } ?>
                        	html += \'  </td>\';	';
        $this->load->helper( 'modifier' );
        $template_buffer = Modifier::modifyStringBuffer( $template_buffer,$search,$add,'before' );

        $search = 'html += \'  <td class="text-left" style="width: 30%;"><input type="text" name="banner_image[\' + image_row + \'][link]" value="" placeholder="<?php echo $entry_link; ?>" class="form-control" /></td>\';';
        $add  = '  html += \'  <td class="text-left">\';
                        	<?php foreach ($languages as $language) { ?>
                        	html += \'    <div class="input-group">\';
                        	html += \'      <span class="input-group-addon"><img src="view/image/flags/<?php echo $language[\'image\']; ?>" link_title="<?php echo $language[\'name\']; ?>" /></span><input type="text" name="banner_image[\' + image_row + \'][banner_image_description][<?php echo $language[\'language_id\']; ?>][link_title]" value="" placeholder="<?php echo $entry_link_title; ?>" class="form-control" />\';
                            html += \'    </div>\';
                        	<?php } ?>
                        	html += \'  </td>\';	';
        $this->load->helper( 'modifier' );
        $template_buffer = Modifier::modifyStringBuffer( $template_buffer,$search,$add,'after' );

		return parent::preRender( $template_buffer, $template_name, $data );
	}
}