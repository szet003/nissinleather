<?php
class txt_ControllerCommonMenu extends ControllerCommonMenu {
	public function preRender( $template_buffer, $template_name, &$data ) {
		if ($template_name != 'common/menu.tpl') {
			return parent::preRender( $template_buffer, $template_name, $data );
		}
        $this->load->language('common/menu');
	    //----Thêm Pages vào Menu----//

        //Text & data
        $data['text_pages'] = $this->language->get('text_pages');
        $data['pages'] = $this->url->link('catalog/pages', 'token=' . $this->session->data['token'], true);

		// Sửa view menu
		$search = '<li><a href="<?php echo $product; ?>"><?php echo $text_product; ?></a></li>';
		$add  = '<li><a href="<?php echo $pages; ?>"><?php echo $text_pages; ?></a></li>';
		$this->load->helper( 'modifier' );
            $template_buffer = Modifier::modifyStringBuffer( $template_buffer,$search,$add,'after' );
		return parent::preRender( $template_buffer, $template_name, $data );
	}
}
