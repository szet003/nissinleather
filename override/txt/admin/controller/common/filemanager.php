<?php

class txt_ControllerCommonFileManager extends ControllerCommonFileManager
{
    public function preRender($template_buffer, $template_name, &$data)
    {
        if ($template_name != 'common/filemanager.tpl') {
            return parent::preRender($template_buffer, $template_name, $data);
        }
        $data['mupload_status'] = $this->config->get('mupload_status');

        // Sửa view filemanager.tpl
        $search = '<div class="modal-dialog modal-lg">';
        $add = '<div id="progress" class="progress"><div class="progress-bar progress-bar-success"></div></div>';
        $this->load->helper('modifier');
        $template_buffer = Modifier::modifyStringBuffer($template_buffer, $search, $add, 'before');

        $search = '<i class="fa fa-trash-o"></i></button>';
        $add = ' <span class="btn btn-success fileinput-button" id="muti-image-upload-add" data-toggle="tooltip" title="" data-original-title="Add">
                <i class="glyphicon glyphicon-plus"></i>
                <span></span>
            </span>  
            <input type="checkbox" name="muti-image-upload" id="muti-image-upload" data-toggle="tooltip" title="" data-original-title="Select All">  ';
        $template_buffer = Modifier::modifyStringBuffer($template_buffer, $search, $add, 'after');

        $search = '$(\'#button-upload\').on(\'click\', function() {';
        $offset = 49;
        $add = '$(function(){
                    var muti_image_upload_count,tmp_muti_image_upload_count=0;
                    $("#muti-image-upload").on(\'click\', function(){
                        $("input[name^=path]").prop("checked", $(this).prop("checked"));
                    });
                    $(\'body\').prepend(\'<input id="muti_fileupload" type="file" name="files[]" multiple style="display:none;">\');
                    $(\'#button-upload\').on("click", function(){
                        $(\'#muti_fileupload\').trigger(\'click\');
                    })
                    $(\'#muti_fileupload\').fileupload({
                            url: \'index.php?route=common/filemanager/mupload&token=<?php echo $token; ?>&directory=<?php echo $directory; ?>\',
                            dataType: \'json\',
                            done: function (e, data) {
                                tmp_muti_image_upload_count++;
                                if (tmp_muti_image_upload_count==muti_image_upload_count) {
                                    $(\'#button-refresh\').trigger(\'click\');
                                };
                            },
                            change: function(e, data){
                                muti_image_upload_count = data.files.length
                            },
                            drop: function(e, data){
                                muti_image_upload_count = data.files.length
                            },
                            progressall: function (e, data) {
                                var progress = parseInt(data.loaded / data.total * 100, 10);
                                $(\'#progress .progress-bar\').css(
                                    \'width\',
                                    progress + \'%\'
                                );
                            }
                        }).prop(\'disabled\', !$.support.fileInput)
                            .parent().addClass($.support.fileInput ? undefined : \'disabled\');

                    $("#muti-image-upload-add").on("click", function(){
                            $("a.thumbnail").each(function(){
                                var range, sel = document.getSelection(); 
                                console.log(range);
                                if (sel.rangeCount) { 
                                    var img = document.createElement(\'img\');
                                    img.src = $(this).attr(\'href\');
                                    var br = document.createElement(\'br\');
                                    range = sel.getRangeAt(0); 
                                    range.insertNode(img); 
                                    range.insertNode(br);
                                }
                            });
                            $(\'#modal-image\').modal(\'hide\');
                    })   
                }); ';
        $template_buffer = Modifier::modifyStringBuffer($template_buffer, $search, $add, 'replace', $offset);


        return parent::preRender($template_buffer, $template_name, $data);
    }

    public function mupload()
    {
        $this->load->language('common/filemanager');

        $json = array();

        // Check user has permission
        if (!$this->user->hasPermission('modify', 'common/filemanager')) {
            $json['error'] = $this->language->get('error_permission');
        }

        // Make sure we have the correct directory
        if (isset($this->request->get['directory'])) {
            $directory = rtrim(DIR_IMAGE . 'catalog/' . str_replace(array('../', '..\\', '..'), '', $this->request->get['directory']), '/');
        } else {
            $directory = DIR_IMAGE . 'catalog';
        }

        // Check its a directory
        if (!is_dir($directory)) {
            $json['error'] = $this->language->get('error_directory');
        }

        if (!$json && !isset($this->request->get['file'])) {
            if (!empty($this->request->files['files']['name'][0]) && is_file($this->request->files['files']['tmp_name'][0])) {
                // Sanitize the filename
                $filename = basename(html_entity_decode($this->request->files['files']['name'][0], ENT_QUOTES, 'UTF-8'));

                // Validate the filename length
                if ((utf8_strlen($filename) < 3) || (utf8_strlen($filename) > 255)) {
                    $json['error'] = $this->language->get('error_filename');
                }

                // Allowed file extension types
                $allowed = array(
                    'jpg',
                    'jpeg',
                    'gif',
                    'png'
                );

                if (!in_array(utf8_strtolower(utf8_substr(strrchr($filename, '.'), 1)), $allowed)) {
                    $json['error'] = $this->language->get('error_filetype');
                }

                // Allowed file mime types
                $allowed = array(
                    'image/jpeg',
                    'image/pjpeg',
                    'image/png',
                    'image/x-png',
                    'image/gif'
                );

                if (!in_array($this->request->files['files']['type'][0], $allowed)) {
                    $json['error'] = $this->language->get('error_filetype');
                }

                // Check to see if any PHP files are trying to be uploaded
                $content = file_get_contents($this->request->files['files']['tmp_name'][0]);

                if (preg_match('/\<\?php/i', $content)) {
                    $json['error'] = $this->language->get('error_filetype');
                }

                // Return any upload error
                if ($this->request->files['files']['error'][0] != UPLOAD_ERR_OK) {
                    $json['error'] = $this->language->get('error_upload_' . $this->request->files['files']['error'][0]);
                }
            } else {
                $json['error'] = $this->language->get('error_upload');
            }
        }

        if (!$json) {
            move_uploaded_file($this->request->files['files']['tmp_name'][0], $directory . '/' . $filename);
            $this->load->model('tool/image');
            $tmp['name']  = $filename;
            $tmp['url']  =  ltrim(str_replace(str_replace('\\', '/',DIR_IMAGE), '', str_replace('\\', '/', $directory)), '/') . $filename;
            $tmp['deleteUrl']  = 'DELETE';
            $tmp['thumbnailUrl']  = $this->model_tool_image->resize(utf8_substr($directory . '/' . $filename, utf8_strlen(DIR_IMAGE)), 100, 100);
            $ret['files'][] = $tmp;
        }
        $this->response->addHeader('Pragma: no-cache');
        $this->response->addHeader('Cache-Control: no-store, no-cache, must-revalidate');
        $this->response->addHeader('Content-Disposition: inline; filename="files.json"');
        // Prevent Internet Explorer from MIME-sniffing the content-type:
        $this->response->addHeader('X-Content-Type-Options: nosniff');
        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($ret));
    }
}
