<?php
class txt_ControllerCommonHeader extends ControllerCommonHeader {
	public function preRender($template_buffer, $template_name, &$data)
	    {
	        if ($template_name != 'common/header.tpl') {
	            return parent::preRender($template_buffer, $template_name, $data);
	        }


	        $search = '</head>';
	        $add = '<script type="text/javascript" src="view/javascript/stringToSlug/speakingurl.min.js"></script>
	        		<script type="text/javascript" src="view/javascript/stringToSlug/jquery.stringToSlug.min.js"></script>';
	        $this->load->helper('modifier');
	        $template_buffer = Modifier::modifyStringBuffer($template_buffer, $search, $add, 'before');

	        return parent::preRender($template_buffer, $template_name, $data);
	    }
}
