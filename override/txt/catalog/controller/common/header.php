<?php

class txt_ControllerCommonHeader extends ControllerCommonHeader
{
    public function preRender($template_buffer, $template_name, &$data)
    {
        if (!$this->endsWith($template_name, '/template/common/header.tpl')) {
            return parent::preRender($template_buffer, $template_name, $data);
        }

        // Menu
        $this->load->model('catalog/category');
        $data['categories'] = array();

        $categories = $this->model_catalog_category->getCategories(0);

        foreach ($categories as $category) {
            if ($category['top']) {
                // Level 2
                $children_data = array();

                $children = $this->model_catalog_category->getCategories($category['category_id']);

                foreach ($children as $child) {
                    // Level 3
                    $children_3rd_data = array();
                    $children_3rd = $this->model_catalog_category->getCategories($child['category_id']);
                    foreach ($children_3rd as $child_3rd) {
                        $children_3rd_data[] = array(
                            'name' => $child_3rd['name'],
                            'href' => $this->url->link('product/category', 'path=' . $category['category_id'] . '_' . $child_3rd['category_id'])
                        );
                    }

                    $children_data[] = array(
                        'parent_name' => $category['name'],
                        'parent_link' => $this->url->link('product/category', 'path=' . $category['category_id']),
                        'name' => $child['name'],
                        'href' => $this->url->link('product/category', 'path=' . $category['category_id'] . '_' . $child['category_id']),
                        'children' => $children_3rd_data
                    );

                }

                // Level 1
                $data['categories'][] = $children_data;

            }
        }



        $theme_folder = $this->config->get('theme_default_directory');
        $data['theme_path'] = $data['base'] . 'catalog/view/theme/' . $theme_folder . '/';

        // For page specific css
        if (isset($this->request->get['route'])) {
            if (isset($this->request->get['product_id'])) {
                $class = '-' . $this->request->get['product_id'].' template-product';
            } elseif (isset($this->request->get['path'])) {
                $class = '-' . $this->request->get['path'].' template-collection';
            } elseif (isset($this->request->get['manufacturer_id'])) {
                $class = '-' . $this->request->get['manufacturer_id'];
            } elseif (isset($this->request->get['information_id'])) {
                $class = '-' . $this->request->get['information_id'];
            } elseif (isset($this->request->get['pages_id'])) {
                $class = 'pages template-page';
            } else {
                $class = '';
            }

            $data['class'] = str_replace('/', '-', $this->request->get['route']) . $class;
        } else {
            $data['class'] = 'page-highest-quality-leather-goods-and-accessories-made-in-the-usa template-index';
        }
        return parent::preRender($template_buffer, $template_name, $data);
    }

    private function endsWith($haystack, $needle)
    {
        if (strlen($haystack) < strlen($needle)) {
            return false;
        }
        return (substr($haystack, strlen($haystack) - strlen($needle), strlen($needle)) == $needle);
    }
}
