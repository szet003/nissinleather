<?php
class txt_ControllerCommonHome extends ControllerCommonHome {
	public function preRender( $template_buffer, $template_name, &$data ) {
		if (!$this->endsWith( $template_name, '/template/common/home.tpl' )) {
			return parent::preRender( $template_buffer, $template_name, $data );
		}
		$this->load->model('catalog/pages');
        $this->load->model('tool/image');

        $data['pagess'] = array();

        $pagess = $this->model_catalog_pages->getPagess(0);

        foreach ($pagess as $pages) {
            if ($pages['home']) {
                // Level 1
                $data['pagess'][] = array(
                    'name'     => $pages['name'],
                    'column'   => $pages['column'] ? $pages['column'] : 1,
                    'href'     => $this->url->link('pages/pages', 'pages_id=' . $pages['pages_id']),
                    'image'    => $this->model_tool_image->resize($pages['image'], 900, 675 )
                );
            }
        }

		return parent::preRender( $template_buffer, $template_name, $data );
	}

	private function endsWith( $haystack, $needle ) {
		if (strlen( $haystack ) < strlen( $needle )) {
			return false;
		}
		return (substr( $haystack, strlen($haystack)-strlen($needle), strlen($needle) ) == $needle);
	}
}