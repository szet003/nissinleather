<?php
class txt_ControllerCommonFooter extends ControllerCommonFooter {
	public function preRender( $template_buffer, $template_name, &$data ) {
		if (!$this->endsWith( $template_name, '/template/common/footer.tpl' )) {
			return parent::preRender( $template_buffer, $template_name, $data );
		}
		if ($this->request->server['HTTPS']) {
			$server = $this->config->get('config_ssl');
		} else {
			$server = $this->config->get('config_url');
		}

		if (is_file(DIR_IMAGE . $this->config->get('config_icon'))) {
			$this->document->addLink($server . 'image/' . $this->config->get('config_icon'), 'icon');
		}

		$data['base'] = $server;
        $theme_folder = $this->config->get('theme_default_directory');
        $data['theme_path'] = $data['base'].'catalog/view/theme/'.$theme_folder.'/';

		return parent::preRender( $template_buffer, $template_name, $data );
	}

	private function endsWith( $haystack, $needle ) {
		if (strlen( $haystack ) < strlen( $needle )) {
			return false;
		}
		return (substr( $haystack, strlen($haystack)-strlen($needle), strlen($needle) ) == $needle);
	}
}
