<?php

class txt_ControllerProductProduct extends ControllerProductProduct
{
    public function preRender($template_buffer, $template_name, &$data)
    {
        if (!$this->endsWith($template_name, '/template/product/product.tpl')) {
            return parent::preRender($template_buffer, $template_name, $data);
        }

        $data['text_related_products'] = $this->language->get('text_related_products');

        if (isset($this->request->get['product_id'])) {
            $product_id = (int)$this->request->get['product_id'];
        } else {
            $product_id = 0;
        }
        $this->load->model('catalog/product');
        $this->load->model('catalog/category');

        $path = $this->model_catalog_product->getProductCategoryPath($product_id);
        $paths = explode('_', $path);
        if (count($paths) > 2) {
            $path = $paths[0];
        }

        $category_info = $this->model_catalog_category->getCategory($path);
        $data['category'] = $category_info['name'];


        $product_info = $this->model_catalog_product->getProduct($product_id);
        $data['current'] = $this->url->link('product/product', 'product_id=' . $this->request->get['product_id']);
        $data['details'] = html_entity_decode($product_info['details'], ENT_QUOTES, 'UTF-8');
        $data['shipping_details'] = html_entity_decode($product_info['shipping_details'], ENT_QUOTES, 'UTF-8');
        $data['phone'] = $this->config->get('config_telephone');

        $this->load->model('tool/image');
        if ($product_info['big_image']) {
            $data['big_image'] = $this->model_tool_image->resize($product_info['big_image'], $this->config->get($this->config->get('config_theme') . '_big_image_product_width'), $this->config->get($this->config->get('config_theme') . '_big_image_product_height'));
        } else {
            $data['big_image'] = '';
        }


        $data['products'] = array();

        $results = $this->model_catalog_product->getProductRelated($this->request->get['product_id']);
        $order = 1;

        foreach ($results as $result) {
            if ($result['image']) {
                $image = $this->model_tool_image->resize($result['image'], $this->config->get($this->config->get('config_theme') . '_image_related_width'), $this->config->get($this->config->get('config_theme') . '_image_related_height'));
            } else {
                $image = $this->model_tool_image->resize('placeholder.png', $this->config->get($this->config->get('config_theme') . '_image_related_width'), $this->config->get($this->config->get('config_theme') . '_image_related_height'));
            }

            $data['products'][] = array(
                'order' => $order,
                'handle' => $result['product_id'],
                'title' => $result['name'],
                'imgsrc' => $image,
            );
            $order++;
        }

        $data['products'] = json_encode($data['products']);

        //print_r(json_encode($data['products']));
        //die;
        return parent::preRender($template_buffer, $template_name, $data);
    }

    function get_product_related()
    {
        if (isset($this->request->get['product_id'])) {
            $product_id = (int)$this->request->get['product_id'];
        } else {
            $product_id = 0;
        }
        $this->load->model('catalog/product');
        $this->load->model('catalog/category');

        $path = $this->model_catalog_product->getProductCategoryPath($product_id);
        $paths = explode('_', $path);
        if (count($paths) > 2) {
            $path = $paths[0];
        }

        $category_info = $this->model_catalog_category->getCategory($path);
        $data['category'] = $category_info['name'];

        $product_info = $this->model_catalog_product->getProduct($product_id);

        if ($product_info) {
            $this->load->model('tool/image');

            //$path = $this->model_catalog_product->getProductCategoryPath($product_id);

            if ($product_info['image']) {
                $data['popup'] = $this->model_tool_image->resize($product_info['image'], $this->config->get($this->config->get('config_theme') . '_image_popup_width'), $this->config->get($this->config->get('config_theme') . '_image_popup_height'));
            } else {
                $data['popup'] = '';
            }

            if ($product_info['image']) {
                $data['thumb'] = $this->model_tool_image->resize($product_info['image'], $this->config->get($this->config->get('config_theme') . '_image_thumb_width'), $this->config->get($this->config->get('config_theme') . '_image_thumb_height'));
            } else {
                $data['thumb'] = '';
            }

            if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
                $data['price'] = $this->currency->format($this->tax->calculate($product_info['price'], $product_info['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
            } else {
                $data['price'] = false;
            }

            $data['product_info'] = array(
                'title' => $product_info['name'],
                'url' => $this->url->link('product/product', 'path=' . $path . '&product_id=' . $product_id),
                'images' => array($data['thumb']),
                'model' => $product_info['model'],
                'category' => $data['category'],
                'price' => $data['price']

            );
        }

        print_r(json_encode($data['product_info']));

    }

    private
    function endsWith($haystack, $needle)
    {
        if (strlen($haystack) < strlen($needle)) {
            return false;
        }
        return (substr($haystack, strlen($haystack) - strlen($needle), strlen($needle)) == $needle);
    }
}
